///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////--calculates the delivery rate of shop --////////////////////////////////////////////////

var globalDel;
var delRate;


async function finalCost(costofItems) {
    var x;
    var y;
    var c;
    //--geting the user location details---------------------------------------------------------------------------------------//

    var p = await actvServ();
    var cost = await tabulateTotals();
            var shopDeliveryRadius = p.deliveryRadius
            var p = p.lonlat;
            var str = p;
            
            x = str.split(",")[0];
            y = str.split(",")[1];

            //Calculate Promo Discount
            //TO-DO
            //var getProdPrice = document.getElementById("totals").innerHTML;
           // promoDiscount = (dis / 100) * getProdPrice
            //$("#promoDiscount").html('<span id="dscnt" style="font-size:2em;">' + promoDiscount + '</span><br>money <br> back');

            if (instorePickup == true) {
                var coords = {latitude: x, longitude: y};
            } else {
                var r = await getLoc();
                
                var coords = r.coords;
                    
            }       
                    
                    
                    //--geting the shop delivery Rates---------------------------------------------------------------------------------------//
                    //var distance =getDistanceFromLatLonInKm(from-lat,from-long,to-lat,from-long);
                    getDistanceFromLatLonInKm(coords.latitude, coords.longitude, x, y).then(function(distance) {
                        $("#ScheduleO").removeAttr("disabled");
                         $("#ConfirmO").removeAttr("disabled");

                        var dist = distance
                        var deliveryRadius={};
                        
                            deliveryRadius.max = shopDeliveryRadius.max?shopDeliveryRadius.max:150;
                            deliveryRadius.min = shopDeliveryRadius.min?shopDeliveryRadius.min:0.5;
                            
                        if (dist > deliveryRadius.max) {
                            console.log("distance is ", dist)
                            console.log("max distance is ", deliveryRadius.max)
                            M.toast({
                                html: 'Ooops! You are out of delivery range..'
                            })
                            M.Modal.init(document.getElementById('modalconfirm')).close();
                            clearCart();
                        } else {
                            //--rates
                            var rates = Math.ceil(delRate * distance);
                            //if (rates < 100) {
                            //    rates = 100
                            //}

                            globalDel = rates;

                            $(".totals2").html(numberify(cost) + '<span class=""> '+baseCd+'</span>');
                            
                            $("#inStorePickup").html(numberify(rates) + '<span class=""> '+baseCd+'</span>');
                            
                            $(".confirmText").html(numberify(cost + rates) + '<span class=""> '+baseCd+'</span>');
                        }
                    });
                
          
        
}


//Delivery Settings
function delAvail() {
    var delAval = $("#delAvail").is(':checked');
    if (checkanon() == false) {
        M.Modal.getInstance(document.getElementById('loginModal'), {
            onCloseEnd: function() {
                $(".delivery").click()
            }
        }).open();
        return;
    }
    if ($("#delAvail").is(':checked') == true) {
        navigator.permissions.query({
            name: 'push',
            userVisibleOnly: true
        }).then(function(e) {
            if (e.state == "granted") {
                doFetch({
                    action: 'manageDelAvail',
                    uid: localStorage.getItem('bits-user-name'),
                    sid: localStorage.getItem('bits-active-service'),
                    state: delAval
                }).then(function(e) {
                    if (e.status == "ok") {
                        M.toast({
                            html: 'State changed successfully'
                        })
                    } else {
                        M.toast({
                            html: 'Error! Try again later'
                        })
                    }
                })
            } else {
                document.getElementById('notificationsModal').style.display = "block";
                if ($("#delAvail").is(':checked') == true) {
                    $("#delAvail").prop("checked", false);
                }
            }
        }).catch(function(e) {
            document.getElementById('notificationsModal').style.display = "block";
            if ($("#delAvail").is(':checked') == true) {
                $("#delAvail").prop("checked", false);
            }
        })
    } else {
        doFetch({
            action: 'manageDelAvail',
            uid: localStorage.getItem('bits-user-name'),
            sid: localStorage.getItem('bits-active-service'),
            state: delAval
        }).then(function(e) {
            if (e.status == "ok") {
                M.toast({
                    html: 'State changed successfully'
                })
            } else {
                M.toast({
                    html: 'Error! Try again later'
                })
            }
        })
    }
}
