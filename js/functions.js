///.........................................checks if the payments option for the merchant is on or off ........................................................./////
var promoDiscount;
var deliveryRadius;
var get_orderArrayy;
var get_loc;
var get_locStr;
var get_pointsEarned;
var insufficientOrderNum;
var wishShareId;
var buywishlist;
var promoCheckoutModal = false;
var promoModalActive = false;
var openCheckoutModal = false;
var loadedServiceProfile = false;
var triggerDeliveryModal = false;
var delTimePicker;
var delDatePicker;
var delTime;
var delDate;


async function doSchedule(){
    await doScheduleDate();
    //await doScheduleTime();
}

async function setSchedule(time){
     delTime = time;
}

async function doScheduleTime(date){
    delDate = date;
document.querySelector('#timepicker').innerHTML = '';

 
    var node = document.createElement("input");
node.classList.add('timepicker');
//node.readOnly = true;

   node.setAttribute("type", "text");
document.querySelector('#timepicker').append(node);

    var options = {autoClose: true, onCloseEnd: doConfPay, onSelect: setSchedule};
     
     shopData.lastSchedule = true;
     
    M.Timepicker.init(document.querySelectorAll('.timepicker')[0], options).open();
}

async function doScheduleDate(){
    
var orderRangeStart = '';

var orderRangeStop = '';
    if(getBitsWinOpt('pid')){
   var promoDet =  shopData.shopPromos.filter(obj => {
  return obj.id === parseInt(getBitsWinOpt('pid'))
})[0];

orderRangeStart = promoDet.promoStart;
orderRangeStop = promoDet.promoStop;


}

if ((new Date().getTime()+1000) > new Date(orderRangeStart).getTime()){
    var startTime = new Date(new Date().setDate(new Date().getDate() + 1));
}    else {
    var startTime = new Date(orderRangeStart);
}
    
document.querySelector('#datepicker').innerHTML = '';

    var node = document.createElement("input");
node.classList.add('datepicker');
//node.readOnly = true;
   node.setAttribute("type", "text");
document.querySelector('#datepicker').append(node);

    var options = {autoClose: true, defaultDate: new Date().setDate(new Date().getDate() + 1), minDate: startTime, maxDate: new Date(orderRangeStop), onSelect: doScheduleTime };
     
     
    M.Datepicker.init(document.querySelectorAll('.datepicker')[0], options).open();
   

}

async function doMakeOrder(orderArrayy, res, globalDel, locOrigin, uid, addrr, sid) {
    if (locOrigin == undefined || locOrigin == 'instore') {

        locOrigin = 'instore';
        addrr = 'instore';
        globalDel = 0;
    }
    
    try{
        var xrate = allTokens[shopData.chainContract.account].rate * baseX;
    }catch(e){
        var xrate = 0;
    }
    var proTotal = await tabulateTotals();
    var trackEndPoint = await startPushManager();
    var e = await doFetch({
        action: 'makeOrder',
        data: orderArrayy,
        schedule: {
            "date": delDate,
            "time": delTime
            
        },
        trHash: res,
        trackNote: trackEndPoint,
        delPrice: globalDel,
        loc: locOrigin,
        user: uid,
        locStr: addrr,
        pointsEarned: {
            "contract": shopData.chainContract.account,
            //"coins": document.querySelectorAll("#dscnt")[0].innerHTML / (allTokens[enterpriseContract].rate * baseX),
            "coins": 0,
            "rate": xrate,
            "fiat": baseCd,
            "action": "purchase"
        },
        service: sid,
        proPrice: parseInt(proTotal)
    });
    $("#appendPushSubs").remove();
    if (e.status == "ok") {
        if ($('#modalconfirm').hasClass('activeWishlist') == true) {
            $('#shareWishlist').html('share');

        } else {
            M.toast({
                html: 'Waiting for escrow payment..',
            });
            //clearCart();
        }
        return e;
    } else {
        //swal("Cancelled", "your order is not sent", "error");
        M.toast({
            html: 'error creating order. please try again later!'
        })
        return e;
    }
}

async function payUsingAlternate(payVia, oid) {

var proTotal = await tabulateTotals();
if (instorePickup == true) {
   
   var amount=parseFloat(proTotal);
                                        
} else {
                                        
    var amount=parseFloat(parseFloat(proTotal) + globalDel);
                                
}

 var amount=amount/baseBTC;
 
if (payVia == "btc") {
                                    
placeLNDlogo();

}else if (payVia == "transcode"){
    
    var cols = document.querySelectorAll('.transCodeOption');

    [].forEach.call(cols, (e)=>{
        e.innerHTML='<div class="preloader-wrapper big active" style=" width: 20px; height: 20px; margin-top: 9px;"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div></div><div class="gap-patch"> <div class="circle"></div></div><div class="circle-clipper right"> <div class="circle"></div></div></div></div>';
    });
   
}


    //Refer to store categories list for further documentation
    // /soko/readme.md
    //default payment method can be btc, transcode, creditcard, mobilemoney, directAuth
    //var payVia='btc';

    if (shopCategory == '7') {
        // in this category, all service delivery costs are subsidized
        // allows the customer to pay for service using transit time points
        actii = 'sell';
        var amount=(parseFloat(proTotal - globalDel)/baseBTC);
                                

    } else {
        
        actii = 'buy';
        
    }
    
    // linit rate to make sure customer does not get fewer merchant ASA tokens
        // allTokens[shopData.chainContract.account].rate * baseX
        var limitRate = 0; 
        
        
    if (actii == 'sell') {
        
        
        doFetch({
            action: 'getInsufficientFundsOrderbook',
            contract: shopData.chainContract.account,
            oid: shopData.lastOid,
            rate: limitRate,
            total: amount,
            via:payVia,
            act: actii,
            countryCode: baseCd
        }).then(function(e) {
            if (e.status == "ok") {
                
                 return prepPay(payVia,e);
            } else {
                M.toast({
                    html: "Unable to complete transaction"
                });
                clearCart();
            }
        });

    } else {
        doFetch({
            action: 'getInsufficientFundsOrderbook',
            contract: shopData.chainContract.account,
            oid: shopData.lastOid,
            rate: limitRate,
            via:payVia,
            total: amount,
            act: actii,
            countryCode: baseCd
        }).then(function(e) {
            if (e.status == "ok") {
                

                 return prepPay(payVia,e);
                
            } else {
                M.toast({
                    html: "Unable to complete transaction"
                });
                clearCart();
            }
        });
    }
}


function addQrText(t,el){
  el.innerHTML="";
       

          var qrcodesvg 	= new Qrcodesvg( t, el, 230, {"ecclevel" : 2});

			//add a bevel effect on patterns with a radius of 5.
			//apply one of these three colors to patterns
			qrcodesvg.draw({"method":"round", "radius" : 3, "fill-colors":["#083440","#4c9fb7","#0f5f76"]}, {"stroke-width":1});
         
}


function prepPay(pv,e){
     switch (pv) {
      case 'btc': 
            var payto=e.data.inv.payment_request;
            
            var elm = document.getElementById("altPayQrcode");
            elm.innerHTML="tap to open app";
            var elm = document.getElementById("doPayQrcode");
            
            elm.style.opacity = "1";
            elm.setAttribute("onclick", 'location.href="lightning:'+payto+'"');
           addQrText(payto,elm);
           
           //-- ADD Third party BTC Handlers
           //
           
           try{
               
              
            var payHandlers = e.data.payHandles;
            
            for(var i in payHandlers){
                var nM = payHandlers[i].name;
                var id = payHandlers[i].id;
                var url = payHandlers[i].url;
                
            var elm = document.querySelector('.transCodeOption-'+id);
    
    elm.innerHTML=nM;
    elm.setAttribute("onclick", 'location.href="'+url+'"');

            } 
           }catch(er){
            
            console.warn('Error adding third party BTC Payment options! ', er);   
               
           }
            
            
                return true;
      break;
      case 'transcode': 
           //var payto=e.data.num
            //document.querySelector("#insufficientOrder").removeAttribute("onclick");
            
            
           // $("#creditTopupNo").html(payto);
                return true;
      break;
        default:
            
     console.log('unknown payment method: ',e)
		}  
	

    
}

//Check Browser Compatibility
function checkBrowser() {
    if (!!window.chrome) {
        console.log("Browser compatible")
    } else {
        M.Modal.init(document.getElementById('checkBrowser')).options.dismissible = false
        M.Modal.init(document.getElementById('checkBrowser')).open()
    }
}

function checkPayments() {
    actvServ().then(function(p) {
        try {

            var p = p.payments;
        } catch (e) {
            console.log(e);
            var p = false;
        }
        if (p) {
            ////console.log("payments on")
            $("#paymentBTN").removeClass("displayNone")
        } else {
            //console.log("payments off")
            $(".chat-outs").addClass("displayNone")
            $("#paymentBTN").addClass("displayNone")
            $("#promopriced").addClass("displayNone")
            // $("#bitsPrice").addClass("displayNone")
            //removes the button
            $(".floatingPrice").html("")
            $(".floatingPrice").addClass("pointerNone")
            //adds class with no side panel activatr
            $(".floatingPrice").append('<a href="#" class="bitswaves-effect waves-block bits bitb waves-light chat-collapse btn-floating btn-large "style="pointer-events: none;"><span id="totals" class="totals"></span></a>')
        }
    })
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function callMerchant() {
    try {
        actvServ().then(function(x) {
            $('#appendCallBtn').html('');

    if(x.phone!=null){
    $('.merchantCallBtn').html('').prepend('<div href="tel:' + x.phone + '" id="merchPhoneNo" value="" class="" style="width:auto;line-height: normal;padding: 10px;margin-top: 7px;background:#ffffff ; color: ' + storeTheme + ';margin-top: 14px;border-radius: 2px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 480.56 480.56" style="enable-background:new 0 0 480.56 480.56;width: 23px;color: ' + storeTheme + ';position: absolute;" xml:space="preserve"> <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8 c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4 c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8 c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3 c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9 c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z" fill="' + storeTheme + '"></path> <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1 c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z" fill="#FFFFFF"></path> <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5 l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z" fill="#FFFFFF"></path> </svg><span style=" padding-left: 40px;">Call</span></div>');
                       
    
    }
            
            try {
                $('#appendCallBtn').append('<a href="/soko/#s=' + localStorage.getItem("bits-active-service") + '" class="manage-store" id="manage-store" style="margin-right: 10px;display: block;float:  right;"></a><button id="share" value="Share" class="bitb noshadow" style="float: right !important; line-height: normal; margin-right: 3px; border: none; background: none; padding: 10px;"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 473.932 473.932" style="enable-background:new 0 0 473.932 473.932;width: 20px; margin-top: 10px;" xml:space="preserve"><path d="M385.513,301.214c-27.438,0-51.64,13.072-67.452,33.09l-146.66-75.002 c1.92-7.161,3.3-14.56,3.3-22.347c0-8.477-1.639-16.458-3.926-24.224l146.013-74.656c15.725,20.924,40.553,34.6,68.746,34.6 c47.758,0,86.391-38.633,86.391-86.348C471.926,38.655,433.292,0,385.535,0c-47.65,0-86.326,38.655-86.326,86.326 c0,7.809,1.381,15.229,3.322,22.412L155.892,183.74c-15.833-20.039-40.079-33.154-67.56-33.154 c-47.715,0-86.326,38.676-86.326,86.369s38.612,86.348,86.326,86.348c28.236,0,53.043-13.719,68.832-34.664l145.948,74.656 c-2.287,7.744-3.947,15.79-3.947,24.289c0,47.693,38.676,86.348,86.326,86.348c47.758,0,86.391-38.655,86.391-86.348 C471.904,339.848,433.271,301.214,385.513,301.214z" fill="#FFFFFF"></path></svg> </button>');
                $('#appendCallBtn').append('<a href="#pendingOrderModal" class="modal-trigger pendingOdersActive" style="float: right; line-height: inherit;height: -webkit-fill-available;"></a>');
                $('#appendCallBtn').append('<a id="initFeedbackModal" href="#feedbackModal" onclick="feedback();" style="float: right; width: 27px; padding-top: 17px; line-height: initial; margin: 0 30px;"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 480.56 480.56" style="enable-background:new 0 0 480.56 480.56;width: 23px;" xml:space="preserve"> <path d="M365.354,317.9c-15.7-15.5-35.3-15.5-50.9,0c-11.9,11.8-23.8,23.6-35.5,35.6c-3.2,3.3-5.9,4-9.8,1.8 c-7.7-4.2-15.9-7.6-23.3-12.2c-34.5-21.7-63.4-49.6-89-81c-12.7-15.6-24-32.3-31.9-51.1c-1.6-3.8-1.3-6.3,1.8-9.4 c11.9-11.5,23.5-23.3,35.2-35.1c16.3-16.4,16.3-35.6-0.1-52.1c-9.3-9.4-18.6-18.6-27.9-28c-9.6-9.6-19.1-19.3-28.8-28.8 c-15.7-15.3-35.3-15.3-50.9,0.1c-12,11.8-23.5,23.9-35.7,35.5c-11.3,10.7-17,23.8-18.2,39.1c-1.9,24.9,4.2,48.4,12.8,71.3 c17.6,47.4,44.4,89.5,76.9,128.1c43.9,52.2,96.3,93.5,157.6,123.3c27.6,13.4,56.2,23.7,87.3,25.4c21.4,1.2,40-4.2,54.9-20.9 c10.2-11.4,21.7-21.8,32.5-32.7c16-16.2,16.1-35.8,0.2-51.8C403.554,355.9,384.454,336.9,365.354,317.9z" fill="#FFFFFF"></path> <path d="M346.254,238.2l36.9-6.3c-5.8-33.9-21.8-64.6-46.1-89c-25.7-25.7-58.2-41.9-94-46.9l-5.2,37.1 c27.7,3.9,52.9,16.4,72.8,36.3C329.454,188.2,341.754,212,346.254,238.2z" fill="#FFFFFF"></path> <path d="M403.954,77.8c-42.6-42.6-96.5-69.5-156-77.8l-5.2,37.1c51.4,7.2,98,30.5,134.8,67.2c34.9,34.9,57.8,79,66.1,127.5 l36.9-6.3C470.854,169.3,444.354,118.3,403.954,77.8z" fill="#FFFFFF"></path> </svg></a><a id="deliveryModalBtn" style="float:right; padding: 10px; padding-right: 20px;"></a>');
                delRate = x.deliveryRate;
            } catch (e) {
                console.log(e);
            }
            
            /*
            //TO-DO
            //show delivery button only when user is a delivery operator for the store
            try {
                showDeliverBtn();
            } catch (err) {
                console.log(err)
            }
            
            
            */
            //getUserOders();

        });
    } catch (err) {
        console.log(err)
    }
}

function rate() {
    $('#RateModal').openModal();
}

function initializeTabs() {
    M.Tabs.getInstance(document.querySelector(".prdTabs")).options.onShow = function(e) {
        var clickedTab = $(e).attr("id")
        if ($(e)[0].childNodes.length == 0) {
            actvServ().then(function(xx){
                
                  var prodList = xx.list
                for (x in prodList) {
                    if (prodList[x].metric == null) {
                        // console.log("no metrics set")
                        prodList[x].metric = "piece";
                    }
                    if (prodList[x].productCategory == clickedTab) {
                        try {
                            var srcSetPth = prodList[x].imagePath.replace('.png', '-35.webp');

                        } catch (e) {
                            var srcSetPth = '';

                        }
                        $("#" + clickedTab + "").append('<li class="collection-item avatar bits-max " style="position: relative;"><div class="row" style="margin-bottom:0px;"><div class="col s2"><img srcset="/' + srcSetPth + ' 35w" src="' + prodList[x].imagePath.replace('.png', '.webp').replace('.webp', '-224.webp') + '" data-caption="' + prodList[x].description + '" alt="" class="circle materialboxed" style="width:35px; height:35px;"></div> <div class="col s5" style="text-align:left;"><span class="title"><span class="serviceListTitle" id="pcat" pcategory""> ' + prodList[x].name + ' </span></span><p style="margin:0px;" class="serviceListFirstline"> <span id="bitsPrice" class="bits-badge bits left">' + numberify(prodList[x].price, '') + ' <span class="localCurr"><span class="conf-curr"></span> </span>per ' + prodList[x].rstQuantity + '  ' + prodList[x].metric + ' </span></p></div><div class="col s5" style="padding:0px;"><div class="handle-counter" data-step="1" data-intro=" Add products to cart here" id="prod-' + prodList[x].id + '-counter" style="width:100% !important;"><div class="row" style="padding: 0 15px;margin-bottom:0px;"> <div class="col s4"><button class="counter-minus bits btn btn-primary btn-floating btn-f pulse"  style="line-height: 5px;margin-top:7px; width: 35px; height: 35px; margin-top: 10px;">-</button></div><div class="col s4"><input id= "bitsInputQty' + prodList[x].id + '" class="bitsInputQty" price="' + prodList[x].price + '" pid="' + prodList[x].id + '" type="text" value="0" min="" style="border-bottom: none;margin-top:6px;"></div><div class="col s4"><button class="counter-plus js--triggerAnimation bits btn btn-primary btn-floating btn-f pulse" style="line-height: 5px; float:right; margin-top: 7px; width: 35px; height: 35px; margin-top: 10px;" >+</button></div></div></div></div></li>');
                        $('#prod-' + prodList[x].id + '-counter').handleCounter();
                    }
                }
                
            });
            
            
        }
    };
}

function checkOrderState() {
    
    
}

function checkPromoBuy() {
    var promotions = shopData.shopPromos
    var promoLength = promotions.length
    if (promoLength => 1) {
        for (id in promotions) {
            if (getBitsWinOpt('pid') == promotions[id].id) {
                $("#totals").parent().addClass("granted");
                
                makeOrder(promotions[id].promoItems);
                //buyPromo(promotions[id].id);
                //promoCheckoutModal = true;
                //promoToBuy = promotions[id].id;
            }
        }
    }
}

//...........................URL check end//.................................................................................................................................................
//function service Page loader..........
function servicePageLoader() {
    servicePageLoader.called = true
    ////console.log('servicePageLoader()..');
    $(".delrow").removeClass("displayNone");
    if (parseInt(shopData.id) > 5) {
        var servID = shopData.id;
    } else {
        var servID = getBitsWinOpt('a');
    }
    document.querySelector("link[rel='manifest']").href = "/bits/web-manifest.json?s=" + servID;
    localStorage.setItem('bits-active-service', servID);
    if (parseInt(shopData.id) == 2) {
        contact();
    }
    if (parseInt(shopData.id) > 2) {
        //merchants options start;
        $(".serviceListHolder").show();
        $(".serviceListCard").show();
        $(".promoHolder").hide();
                    populateService(shopData);
                    populated = true;
        
            setTimeout(function(e) {
                try{
                    M.Tabs.init(document.querySelectorAll('.prdTabs'))

                    //Get Tab Content
                    initializeTabs();
                    
                    checkPromoBuy();
                    checkOrderState();
            }catch(er){
            console.log(er);
        }
                }, 250);
        
               
        //depreciated code
        //used to call server for info. this is now appended to page as above
        
        /*
        var svReq = getObjectStore('data', 'readwrite').get('bits-merchant-id-' + getBitsWinOpt('s'));
        svReq.onsuccess = function(event) {
            try {
                populateService(JSON.parse(event.target.result));
                populated = true;

                setTimeout(function(e) {
                    M.Tabs.init(document.querySelectorAll('.prdTabs'))

                    //Get Tab Content
                    initializeTabs();
                }, 3000)
            } catch (err) {
                fetchServiceProfile.then(function(e) {
                    storeTheme = e.data.theme
                    $("#preloader").fadeOut(1000);
                    try {
                        populateService(e.data);
                        populated = true;

                        var svReq = getObjectStore('data', 'readwrite').put(e.data, 'bits-merchant-id-' + getBitsWinOpt('s'));
                        svReq.onsuccess = function(e) {
                            setTimeout(function(e) {
                                M.Tabs.init(document.querySelectorAll('.prdTabs'))

                                //Get Tab Content
                                initializeTabs();
                            }, 3000)
                        };
                        svReq.onerror = function() {
                            ////console.log('err not saved store info to db')
                        }

                    } catch (err) {
                        console.log(err)
                        if (loadedServiceProfile == false) {
                            setTimeout(function() {
                                servicePageLoader();
                            }, 3000);
                        }
                    }
                    setTimeout(function(e) {
                        $(".bits").css("background-color", storeTheme)
                    }, 2000)
                })
            }
        };
        svReq.onerror = function() {

            ////console.log('service not found in db. perhaps trying from DOM 2');
            $("#preloader").fadeOut(1000);
            fetchServiceProfile.then(function(e) {
                populateService(e.data);
                populated = true;
            })
        }

        fetchServiceProfile = doFetch({
            action: 'serviceProfile',
            id: servID,
            service: getBitsWinOpt('s')
        })

        fetchServiceProfile.then(function(e) {
            if (e.status == "ok") {
                loadedServiceProfile = true;
                shopData = e.data;
                checkPromoBuy(e.data);

                //Check if URL contains new delivery member
                if (checkanon() == true) {
                    if (getBitsOpt('orderData') == 'new') {
                        //If so, open delivery guy modal
                        M.Modal.init(document.getElementById('deliveryGuyModal'), {}).open();
                    }
                    triggerDeliveryModal = false;
                } else {
                    if (getBitsOpt('orderData') == 'new') {
                        triggerDeliveryModal = true;
                    }
                }

                var prdList = e.data.list
                getObjectStore('data', 'readwrite').get('bits-merchant-id-' + getBitsWinOpt('s')).onsuccess = function(event) {
                    if (event.target.result == undefined) {
                        setTimeout(function(e) {
                            $('.allPrds').html('')

                            for (var ii = 0; ii < prdList.length; ++ii) {

                                for (var iii in prodCatArray) {
                                    if (prodCatArray[iii] == prdList[ii].productCategory) {
                                        appendProd(prdList[ii].id);
                                    }
                                }

                                var getThis;

                                function appendProd(e) {
                                    getThis = e;
                                    if (prdList[ii].id != e) {}
                                }
                                if (getThis != prdList[ii].id) {
                                    if (prdList[ii].metric == null) {
                                        // console.log("no metrics set")
                                        prdList[ii].metric = "piece";
                                    }
                                    $('.allPrds').append('<li class="collection-item avatar bits-max " style="position: relative;"><div class="row" style="margin-bottom:0px;"><div class="col s2"><img srcset="' + prdList[ii].imagePath + ' 35w" src="' + prdList[ii].imagePath + '" data-caption="' + prdList[ii].description + '" alt="" class="circle materialboxed" style="width:35px; height:35px;"></div> <div class="col s5" style="text-align:left;"><span class="title"><span class="serviceListTitle" id="pcat" pcategory""> ' + prdList[ii].name + ' </span></span><p style="margin:0px;" class="serviceListFirstline"> <span id="bitsPrice" class="bits-badge bits left">' + numberify(prdList[ii].price, '') + ' <span class="localCurr"><span class="conf-curr"></span> </span>per ' + prdList[ii].metric + ' </span></p></div><div class="col s5" style="padding:0px;"><div class="handle-counter" data-step="1" data-intro=" Add products to cart here" id="prod-' + prdList[ii].id + '-counter" style="width:100% !important;"><div class="row" style="padding: 0 15px;margin-bottom:0px;"> <div class="col s4"><button class="counter-minus bits btn btn-primary btn-floating btn-f pulse"  style="line-height: 5px;margin-top:7px; width: 35px; height: 35px; margin-top: 10px;">-</button></div><div class="col s4"><input id= "bitsInputQty' + prdList[ii].id + '" class="bitsInputQty" price="' + prdList[ii].price + '" pid="' + prdList[ii].id + '" type="text" value="0" min="" style="border-bottom: none;margin-top:6px;"></div><div class="col s4"><button class="counter-plus js--triggerAnimation bits btn btn-primary btn-floating btn-f pulse" style="line-height: 5px; float:right; margin-top: 7px; width: 35px; height: 35px; margin-top: 10px;" >+</button></div></div></div></div></li>');
                                    $('#prod-' + prdList[ii].id + '-counter').handleCounter();
                                }
                            }
                        }, 1000);
                    }
                }

                deliveryRadius = e.data.deliveryRadius
                var svReq = getObjectStore('data', 'readwrite').put(e.data, 'bits-merchant-id-' + e.data.id);
                svReq.onsuccess = function() {
                    try {
                        if (!populated) {
                            populateService(e.data);
                            populated = true;
                        }
                        setTimeout(function() {
                            fullScreenMode()
                        }, 3000);
                        if (localStorage.getItem("fullScreenPermission") == null) {} else if (localStorage.getItem("fullScreenPermission") == "true") {
                            if ($(".fullscreenToast").length >= 1) {
                                $(".fullscreenToast").remove();
                            }
                            var toastHTML = '<span>Enable fullscreen mode</span><button class="btn-flat toast-action" onclick="fullScreenMode();">ok</button>';
                            M.toast({
                                html: toastHTML,
                                displayLength: 5000,
                                classes: "fullscreenToast"
                            });
                        }
                    } catch (err) {
                        ////console.log('service not found in db. perhaps try loading from server AGAIN!!')
                    }
                };
                svReq.onerror = function() {
                    setTimeout(function() {
                        servicePageLoader();
                    }, 3000);
                }
                if (window.matchMedia('(display-mode: standalone)').matches) {
                    $("#saveStoreHomeScreen").css("display", "none");
                } else {

                }


                //Check Tab To activate

                // if (getBitsOpt("service") == undefined) {
                //
                // } else {
                //     var activeTab = getBitsOpt("service");
                //     var allTabs = $("#prdTabs")[0].childNodes
                //
                //
                //     for (catName in allTabs) {
                //         if(allTabs[catName].innerText == activeTab){
                //             $("#"+activeTab).tab()
                //         }
                //         console.log(allTabs[catName].innerText)
                //     }
                // }
                callMerchant();
            } else {
                $(".serviceListHolder").hide();
                $(".serviceListCard").hide();
                $(".promoHolder").show();
                setTimeout(function() {
                    servicePageLoader();
                }, 3000);
            }
        }).catch(function(err) {
            console.log('error trying to populate from sever ', err);
            var svReq = getObjectStore('data', 'readwrite').get('bits-merchant-id-' + getBitsWinOpt('s'));
            svReq.onsuccess = function(event) {
                try {
                    populateService(event.target.result);
                    populated = true;
                } catch (err) {
                    ////console.log('service not found in db. perhaps trying from DOM 3');
                    var re = /&quot;/gi;
                    var str = document.getElementById('storeMeta').innerHTML;
                    var newstr = str.replace(re, '"');
                    $("#preloader").fadeOut(1000);
                    populateService(JSON.parse(newstr).res);
                    populated = true;
                }
            };
            svReq.onerror = function() {
                //console.log('service not found in db. perhaps trying from DOM 4');
                var re = /&quot;/gi;
                var str = document.getElementById('storeMeta').innerHTML;
                var newstr = str.replace(re, '"');
                $("#preloader").fadeOut(1000);
                try {
                    populateService(JSON.parse(newstr).res);
                    populated = true;
                } catch (err) {
                    console.log(err)
                }

            }
        });
        //merchants options end;
        */
    }
}

function checkServicePageLoader() {
    if (servicePageLoader.called == true) {
        //console.log("do nothing");
        //Check User Phone Number
        doFetch({
            action: 'userVerified',
            uid: localStorage.getItem("bits-user-name")
        }).then(function(e) {
            if (e.status == "ok") {
                if (openCheckoutModal == true) {
                    $('.checkoutTrigger').click();
                    openCheckoutModal = false
                }
                localStorage.setItem('userNumber', e.phone);
            } else if (e.status == "bad") {
                M.Modal.init(document.getElementById('MobileModal')).open();
            } else {
                M.Modal.init(document.getElementById('MobileModal')).open();
            }
        })
    } else {
        servicePageLoader()
    }
}
/*
//Rename Attribute function
jQuery.fn.extend({
    renameAttr: function(name, newName, removeData) {
        var val;
        return this.each(function() {
            val = jQuery.attr(this, name);
            jQuery.attr(this, newName, val);
            jQuery.removeAttr(this, name);
            // remove original data
            if (removeData !== false) {
                jQuery.removeData(this, name.replace('data-', ''));
            }
        });
    }
});
*/
//Materialbox replace srcSetPth
//TO-DO
// remove thumbnails when maximized
/*
$(document).on('click touchstart', '.materialboxed', function(e) {
    var clickedMaterialBox = $(this)
    M.Materialbox.getInstance(clickedMaterialBox).options.onOpenEnd = function(e) {
       // jQuery(e).renameAttr('srcset', 'srcsetd')
    }
    M.Materialbox.getInstance(clickedMaterialBox).options.onCloseEnd = function(e) {
        // jQuery(e).renameAttr('srcsetd', 'srcset')
    }
})
*/
// scroll function....................................................................................................................
// $(window).scroll(function scroll (){
// 	if($('#serviceListCard').hasClass("pin-top")){
// //console.log("not pinned")
// 	}
// 	else{
// 		//console.log("pinned")
// 	}
// }  );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------load name and image of user profile---------------------------------------------------------------------
function loadProfData() {
    var stor = getObjectStore('data', 'readwrite').get('user-profile-' + localStorage.getItem('bits-user-name'));
    stor.onsuccess = function(event) {
        try {
            var upData = JSON.parse(event.target.result);
            $(".username-label").html(upData.name);
            $("#mobileNo").val(upData.phone);
            $(".userProfImg").attr("src", upData.image);
        } catch (err) {
            $(".username-label").html('Anonymous');
            $(".userProfImg").attr("src", '');
            $("#mobileNo").val("");
        }
    };
    stor.onerror = function() {
        $(".username-label").html('Anonymous');
        $(".userProfImg").attr("src", '');
    };
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------load old wallets of user---------------------------------------------------------------------
function loadoldwalletData() {
    var ol = getObjectStore('data', 'readwrite').get('bits-wallets-old-' + localStorage.getItem('bits-user-name'));
    ol.onsuccess = function(event) {
        try {
            var upDat = JSON.parse(event.target.result);
            for (var iii = 0; iii < upDat.length; ++iii) {
                //console.log("old wallets found")
                //var id = upDat[iii].uid ? upDat[iii].uid : 'undefined';
                $('.username-addr-old').append('<span class="title"><a href="#!" id="share" class="secondary-content right"></a></span><span class ="" style="font-size: 12px;">' + upDat.user + '</span>');
            }
        } catch (err) {}
    };
    ol.onerror = function() {};
}
//------------------------------------------------------------------------------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//----------------------------------load name and image of user profile---------------------------------------------------------------------
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//----------------------------------------function to pop up login toast--------------------------------------------------------------------
function togglebuttons() {
    if (checkanon() == false) {
        $("#useAnon").addClass("displayNone");
        $(".call").addClass("displayNone");
    } else {
        $("#useLogin").addClass("displayNone");
        $(".call").removeClass("displayNone");
    }
}
//------------------end function -------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
function showuser() {
    $(".notificationToast").remove()
    $(".signedAsToast").remove()
    if (checkanon() == true) {
        var gtname = getObjectStore('data', 'readwrite').get('user-profile-' + localStorage.getItem('bits-user-name'));
        gtname.onsuccess = function(event) {
            try {
                if (triggerDeliveryModal == true) {
                    M.Modal.init(document.getElementById('deliveryGuyModal'), {}).open();
                }
                M.Modal.init(document.getElementById('loginModal')).close();
                var nam = JSON.parse(event.target.result);
                M.toast({
                    html: '<span class="toastlogin">You are Signed in as: ' + nam.name,
                    displayLength: 1000,
                    classes: 'signedAsToast'
                });
                $(".walletUserUnlock").html('<svg id="userWallet" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 334.877 334.877" style="enable-background:new 0 0 334.877 334.877; width: 24px; float: left; margin-top: 16px;" xml:space="preserve"> <path d="M333.196,155.999h-16.067V82.09c0-17.719-14.415-32.134-32.134-32.134h-21.761L240.965,9.917 C237.571,3.798,231.112,0,224.107,0c-3.265,0-6.504,0.842-9.364,2.429l-85.464,47.526H33.815 c-17.719,0-32.134,14.415-32.134,32.134v220.653c0,17.719,14.415,32.134,32.134,32.134h251.18 c17.719,0,32.134-14.415,32.134-32.134v-64.802h16.067V155.999z M284.995,62.809c9.897,0,17.982,7.519,19.068,17.14h-24.152 l-9.525-17.14H284.995z M220.996,13.663c3.014-1.69,7.07-0.508,8.734,2.494l35.476,63.786H101.798L220.996,13.663z M304.275,302.742c0,10.63-8.651,19.281-19.281,19.281H33.815c-10.63,0-19.281-8.651-19.281-19.281V82.09 c0-10.63,8.651-19.281,19.281-19.281h72.353L75.345,79.95H37.832c-3.554,0-6.427,2.879-6.427,6.427s2.873,6.427,6.427,6.427h14.396 h234.83h17.217v63.201h-46.999c-21.826,0-39.589,17.764-39.589,39.589v2.764c0,21.826,17.764,39.589,39.589,39.589h46.999V302.742z M320.342,225.087h-3.213h-59.853c-14.743,0-26.736-11.992-26.736-26.736v-2.764c0-14.743,11.992-26.736,26.736-26.736h59.853 h3.213V225.087z M276.961,197.497c0,7.841-6.35,14.19-14.19,14.19c-7.841,0-14.19-6.35-14.19-14.19s6.35-14.19,14.19-14.19 C270.612,183.306,276.961,189.662,276.961,197.497z" style="fill: white;"></path> </svg> <div id="checkBal" class="balance-coins" style="position: absolute; left: 55px; padding-top: 3px;width: fit-content !important; text-align: left;">locked</div>');
                $('.unlockWallet').html('<a onclick="selectPaymentMethod();"> <i style="margin-top: 4px; height: 30px;"> <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 334.877 334.877" style="enable-background:new 0 0 334.877 334.877; width: 24px; float: left; margin-top: 6px; margin-left: 3px;" xml:space="preserve"> <path d="M333.196,155.999h-16.067V82.09c0-17.719-14.415-32.134-32.134-32.134h-21.761L240.965,9.917 C237.571,3.798,231.112,0,224.107,0c-3.265,0-6.504,0.842-9.364,2.429l-85.464,47.526H33.815 c-17.719,0-32.134,14.415-32.134,32.134v220.653c0,17.719,14.415,32.134,32.134,32.134h251.18 c17.719,0,32.134-14.415,32.134-32.134v-64.802h16.067V155.999z M284.995,62.809c9.897,0,17.982,7.519,19.068,17.14h-24.152 l-9.525-17.14H284.995z M220.996,13.663c3.014-1.69,7.07-0.508,8.734,2.494l35.476,63.786H101.798L220.996,13.663z M304.275,302.742c0,10.63-8.651,19.281-19.281,19.281H33.815c-10.63,0-19.281-8.651-19.281-19.281V82.09 c0-10.63,8.651-19.281,19.281-19.281h72.353L75.345,79.95H37.832c-3.554,0-6.427,2.879-6.427,6.427s2.873,6.427,6.427,6.427h14.396 h234.83h17.217v63.201h-46.999c-21.826,0-39.589,17.764-39.589,39.589v2.764c0,21.826,17.764,39.589,39.589,39.589h46.999V302.742z M320.342,225.087h-3.213h-59.853c-14.743,0-26.736-11.992-26.736-26.736v-2.764c0-14.743,11.992-26.736,26.736-26.736h59.853 h3.213V225.087z M276.961,197.497c0,7.841-6.35,14.19-14.19,14.19c-7.841,0-14.19-6.35-14.19-14.19s6.35-14.19,14.19-14.19 C270.612,183.306,276.961,189.662,276.961,197.497z" style="fill: black;"></path> </svg> </i><span class="bits-13" style=""></span> <span id="btn">Select Wallet</span></a>');
                $("#ConfirmO").html("Confirm")


                showDeliverBtn();
                if ($('#deliveryModalBtn').hasClass("removeExBtn") == true) {
                    $("#initFeedbackModal").css("display", "none")
                    $("#merchPhoneNo").css("display", "none")
                    showSokoBtn();
                    $("#deliveryModalBtn").css("padding-right", "15px");
                }
            } catch (err) {
                console.log(err)
            }
        };

        navigator.permissions.query({
                name: 'notifications'
            })
            .then(function(permissionStatus) {
                //console.log('geolocation permission state is ', permissionStatus.state);
                if (permissionStatus.state != "granted") {
                    //                    M.toast("Notificatons are turned off <span class='turnOnNtfn' style='color:yellow;'>Turn on</span>", 5000, "notificationToast");
                }

                permissionStatus.onchange = function() {
                    //console.log('geolocation permission state has changed to ', this.state);
                };
            });
        $(document).on("click", ".turnOnNtfn", function() {
            $(".notificationToast").remove();
            Notification.requestPermission(function(result) {
                if (result === 'denied') {
                    //console.log('Permission wasn\'t granted. Allow a retry.');
                    return;
                } else if (result === 'default') {
                    //console.log('The permission request was dismissed.');
                    return;
                }
                //console.log('Permission was granted for notifications');
            });
        });
    } else {
        //showlogintoast()
    }
    if (promoCheckoutModal == true) {
        buyPromo(promoToBuy)
    }
}
//------------------end function -------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------
function showuserNumber() {
    if (shopData == undefined || shopData.phone == null || shopData.phone == '' || shopData.phone == undefined) {
        return false;
    } else {
        return true;
    }
}
//------------------end function -------------------------------------------------------------------------------------
//---------------function to check if wallet is anon----------------------------------------------------------------------------------------------------
function checkanon() {
    if (localStorage.getItem('bits-user-name') == null) {
        sessionStorage.clear();
        return false;
    } else {
        return true;
    }
}
//--------------------------------------end if popup login-----------------------------------------------------------------------------------------
//----------------------------------------------if ststements for popup login modal if user is on anon mode----------------------------------------
//----------------------------------------------function to pop up login modal---------------------------------------------------------------------
function showLogin() {
    startGoogle();
    if (checkanon() == false) {
        M.Modal.init(document.getElementById('loginModal')).open()
    }
    return;
}
//------------------end function -------------------------------------------------------------------------------------
//------------------function to pop up login toast--------------------------------------------------------------------
function showlogintoast() {
    if (checkanon() == false) {

        //        M.toast({
        //            displayLength: 1000000000,
        //            html: '<span class="toastlogin">your wallet is locked</span><button id="toast-wallet-unlocker" onclick="showLogin()" class="btn-flat toast-action" ><span id="toast-wallet-unlocker-sp" style="pointer-events:none;" class="toastloginbutton">Unlock</span></button>'
        //        });
    } else { //showuser()
        //console.log("The user is a valid node!")
    }
}

//TO-DO remove floating array
orderArray = [];
//---------------------------------------function gets the totals of all items on a list----------------------------------------------------------------------------
async function tabulateTotals() {
    ////console.log(this);
    var allProducts = shopData.list;
    var totals = 0;
    
    var pAr= await getPromos();
    //orderArray = [];
    orderArray = pAr;
    
    $('.floatingPrice').addClass('shake'), setTimeout(function() {
        $('.floatingPrice').removeClass('shake')
    }, 1000);
    for (var i = 0,orderArray=orderArray,pAr=pAr; i < allProducts.length; ++i) {
        try {
            var thsVal=document.querySelector("#bitsInputQty"+allProducts[i].id)
            var itVal = thsVal.value ? $(thsVal).val() : 0;
            if (itVal > 0) {
                orderArray.push({
                    pid: thsVal.getAttribute('pid'),
                    discount: 0,
                    count: itVal
                });
                ////console.log(orderArray);
                //Rewards(orderArray);
                $('.recipt').append('');
            }
            
        } catch (err) {
        console.log(err);
        }  
            for (var ii in orderArray){
            if(parseInt(orderArray[ii].pid)==parseInt(allProducts[i].id)){
               orderArray[ii].metric=allProducts[i].metric;
               orderArray[ii].unit=parseFloat(allProducts[i].rstQuantity);
               var preTotal = (allProducts[i].price * parseFloat(orderArray[ii].count) / orderArray[ii].unit);
               
               
        if(getBitsOpt('rid')){
            
            // this is a moneyback promotion so send tokens instead of awarding discount
            // if invalid information is provided, the reward will fail permanently
            // and will be treated as a promotion with 0% discount
            
            
            
        }else{
               
               try{
                   // apply discount to items
                  
                  var preTotal = ((100-orderArray[ii].discount)/100)*preTotal;
                  
                   
               }catch(e){
                   console.warn('WARNING! Unable to apply discount to object. ',e);
               }
               
        }  
               
               
            totals = totals + preTotal;
               
               }
                
            }
          /*
            for (var iii in pAr){
            if(parseInt(pAr[iii].pid)==parseInt(allProducts[i].id)){
               pAr[iii].metric=allProducts[i].metric;
               pAr[iii].unit=parseFloat(allProducts[i].rstQuantity);
            totals = totals + (allProducts[i].price * parseFloat(pAr[iii].count));
               
               }
                
            }
          */
    }
    
     orderArrayy= orderArray;
    // orderArrayy= orderArray.concat(pAr);
     
            $(".recipt").html("");
            $(".totals").html(numberify(totals));
            
            return totals;
        
}

async function deliverItems() {
    
                        instorePickup = false;
                        
                        
                                document.querySelector(".wishlistChip").style.display = "block";
                        //$("#products").html("");
                        delCartChips();
                    //var p = await actvServ();
                            //getLoc(orderLoc).then(function showPosition(e) {
                            //getLoc().then(function showPosition(e) {
                            
                            
                                if ($(".createOrderToast").length >= 1) {
                                    setTimeout(function() {
                                        $(".createOrderToast").remove()
                                    }, 1000)
                                }
                                
                                
                                
                               if(buywishlist){
                          
                                var mapLocc = wishCoords;
                                document.querySelector(".wishlistChip").style.display = "none";
                                orderArrayy = JSON.parse(wishItems);
                                    
                               } else{
                                   
                                
                           var e = await getLoc();
                                //var mapLocc = orderLoc ? orderLoc : e.coords.latitude + ',' + e.coords.longitude;
                                var mapLocc = e.coords.latitude + ',' + e.coords.longitude;
                                document.querySelector(".wishlistChip").style.display = "block";
                               }
                               
                               if(getBitsWinOpt('pid') != undefined){
                                   // wishlist not available for promotions
                                document.querySelector(".wishlistChip").style.display = "none";
                                
                               }
                               
                                //console.log(orderLoc, e, mapLocc);
                                getCoordDet(mapLocc).then(function(mapData) {
                                    
                                    
                                    getProdss(orderArrayy);
                                    locOrigin = mapLocc;
                                
                                try{
                                    locString = mapData[1].results[0].formatted_address;
                                }catch(e){
                                    locString = 'unknown location';
                                }
                                    

                                    console.log(mapData);

                                    get_orderArrayy = orderArray;
                                    get_loc = locOrigin;
                                    get_locStr = locString;
                                    get_pointsEarned = totalKobo;
                                    
                                    //payUsingToken(locOrigin, locString);
                                    //payUsingToken();
                                    //console.log("info! looking for wallet .... ", parseInt(localStorage.getItem('bits-default-wallet')));

                                    $(".confirmText").html("")
                                    $(".confirmText").append()
                                    $(".del").html("")
                                    $(".del").append()
                                    $(".mapText").html("")
                                    $(".orderModalMap").html('<img class="mapdata" src="' + mapData[0] + '" style="width:100%;height: 200px; object-fit: cover;">');
                                    console.log(mapData[1])
                                    $(".mapText").append(locString);
                                    M.Modal.init(document.getElementById('modalconfirm'), {
                                        onOpenEnd: modalConfirmOpenEnd,
                                        onOpenStart: M.Modal.init(document.getElementById('locationModal')).close(),
                                        dismissible: false,
                                        onCloseStart: clearCart
                                    }).open()
                                    if (shopClosed == true) {
                                        M.toast({
                                            html: 'Items will be delivered when the shop opens',
                                            displayLength: 5000
                                        })
                                    }
                                    $('.star2').addClass('animated shake'), setTimeout(function() {
                                        $('.star2').removeClass('animated shake')
                                    }, 1000);
                                    
                                }).catch(function(err) {
                                    console.log(err)
                                    //toast location error
 var toastHTML = '<span>retry location..</span><button class="btn-flat toast-action" onclick="retryDelivery();">ok</button>';
                            M.toast({
                                html: toastHTML,
                                displayLength: 5000
                            });

                                });
                           // });
                       // })
                    }
     
    async function doConfPay() {
        
        if(shopData.lastSchedule){
            var elm = $('#ScheduleO');
        }else{
            var elm = $('#ConfirmO');
        }

                            elm.html('<div class="preloader-wrapper big active" style=" width: 20px; height: 20px; margin-top: 9px;"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div></div><div class="gap-patch"> <div class="circle"></div></div><div class="circle-clipper right"> <div class="circle"></div></div></div></div>')
                                    if (instorePickup == true) {
                                        
                                    var finPAmt=parseFloat($("#totals")[0].innerHTML);
                                        
                                    } else {
                                        
                                        var finPAmt=parseFloat(parseFloat($("#totals")[0].innerHTML) + globalDel);
                                
                                    }
                                    
                                   // var paymode='transcode';
                                   var paymode= M.Tabs.getInstance(document.querySelectorAll('.tabs')[2]).$activeTabLink[0].id.split("-")[1];
    
                           
                            if (sessionStorage.getItem('walletKey')) {
                                //TO-DO remove bits and use default enterprise token
                                // only works on bits-lightning app via shared keys
                                //
                                /*
                                if (((allTokens["0xb72627650f1149ea5e54834b2f468e5d430e67bf"].balance / Math.pow(10, allTokens["0xb72627650f1149ea5e54834b2f468e5d430e67bf"].decimals)) + allTokens["0xb72627650f1149ea5e54834b2f468e5d430e67bf"].totalEarned) * (allTokens["0xb72627650f1149ea5e54834b2f468e5d430e67bf"].rate * baseX) > finPAmt) {
                                    var totCost = finPAmt;
                                    transferTokenValue('0x7D1Ce470c95DbF3DF8a3E87DCEC63c98E567d481', "0xb72627650f1149ea5e54834b2f468e5d430e67bf", finPAmt, allTokens["0xb72627650f1149ea5e54834b2f468e5d430e67bf"].rate).then(function(res) {
                                        console.log(res);
                                        doMakeOrder(orderArrayy, res, globalDel, locOrigin, localStorage.getItem("bits-user-name"), locString, shopData.id).then(function(e) {
                                            console.log(e);
                                        });

                                    }).catch(function(err) {
                                        console.log(err);
                                        
    document.getElementById("insufficientFundsModal").style.display = "block";
     M.Tabs.init(document.querySelectorAll('.tabs')[2], {swipeable: true, onShow: setTimeout(function(e) { payTabOn() }, 350)});
                                        // $("#creditTopup").text($("#delPrdTotal")[0].innerHTML)
                                       payUsingAlternate(paymode);

                                        //$("#tokenMarketLink").html('<a href="/tm/?cid=' + enterpriseContract + '">Buy from Token Market</a>')
                                    })

                                } else {
       document.getElementById("insufficientFundsModal").style.display = "block";
     M.Tabs.init(document.querySelectorAll('.tabs')[2], {swipeable: true, onShow: setTimeout(function(e) { payTabOn() }, 350)});
                                payUsingAlternate(paymode);
                                    //$("#tokenMarketLink").html('<a href="/tm/?cid=' + enterpriseContract + '">Buy from Token Market</a>')
                                }
*/
                            } else {
                                
     var e = await doMakeOrder(orderArrayy, 'invoiced', globalDel, locOrigin, localStorage.getItem("bits-user-name"), get_locStr, shopData.id);
   shopData.lastOid = e.oid;
    document.getElementById("insufficientFundsModal").style.display = "block";
     M.Tabs.init(document.querySelectorAll('.tabs')[2], {swipeable: true, onShow: setTimeout(function(e) { payTabOn() }, 350)});
                               payUsingAlternate(paymode, e.oid);
                            }
                            return false;
                        
                    }

                   //async function payUsingToken(locOrigin, locString) {
                  //    document.querySelector('#ConfirmO').addEventListener("click touchstart", doConfPay); 
                   // }

               
function makeOrder(orderArrayy, orderLoc) {
    locOrigin = 'instore';
    locString = 'instore';
    globalDel = 0;
    updateFinances();
    
    
    if (shopClosed == true) {
        M.toast({
            html: 'Shop closed! Try again later',
            displayLength: 5000
        })
    } else {
        //Rewards();
        //console.log("->", orderArrayy)
        /*
        if (promoModalActive == true) {
            setTimeout(function(e) {
                $('.wishlistChip').css('display', 'none');
            }, 300)
            promoModalActive = false
        } else {
            $('.wishlistChip').css('display', 'block');
        }
        */
        //TO-DO. enable wishlist feature
        
        if (orderArrayy === undefined || orderArrayy.length == 0) {
            M.toast({
                html: "Ooops! You didn't select any product"
            });
            return;
        }
        if (localStorage.getItem('bits-active-service') == null) {
            localStorage.setItem('bits-active-service', getBitsWinOpt('s'));
        }
        var minimumOrder = $("#totals")[0].innerHTML
        $('.delivery').addClass('animated jello');
        //checkanon();
        if (buywishlist == true) {
            buywishlist = false;
        } else {
            //if (checkanon() == false) {
            //    M.Modal.init(document.getElementById('loginModal')).open();
            //    openCheckoutModal = true;
            //    return;
            //}
            /*
            if (localStorage.getItem('userVerifiedNumber') == 'false') {
                function opnCallVer(){
                M.Modal.init(document.getElementById('MobileModal')).open();
                     M.Modal.init(document.getElementById('modalconfirm')).close();
            clearCart();
                }
                
                var toastHTML = '<span>You wont be called when order arrives..</span><button class="btn-flat toast-action walletUserUnlock unlockWalToast" onclick="opnCallVer()">Call me!</button>';
                                                M.toast({
                                                    html: toastHTML,
                                                    displayLength: 4000
                                                });
                
            }
            */
        }
        if (pendingOrders == true) {
            M.Modal.init(document.getElementById('pendingOrderModal')).open()
        } else {
            
            var minOr=100;
            if (shopData.category=="1" && minimumOrder < (minOr*3)) {
                if ($("#totals").parent().hasClass("granted") == true) {} else {
                    M.toast({
                        html: "Ooops! Minimum order is "+baseCd.toUpperCase()+" "+(minOr*3)
                    });
                    
            return;
                }
            }else if (minimumOrder < 100) {
                if ($("#totals").parent().hasClass("granted") == true) {} else {
                    M.toast({
                        html: "Ooops! Minimum order is "+baseCd.toUpperCase()+" "+minOr
                    });
                    
            return;
                }

            } else {
                $("#totals").parent().addClass("granted");
            }
            if ($("#totals").parent().hasClass("granted") == true) {
                $('.spinnerCheckout').css("display", "block");
                $('.checkoutInfo').css("display", "none");
                $(".minOrderToast").remove();
                M.toast({
                    html: 'Creating your order, please wait',
                    displayLength: 100000,
                    classes: "createOrderToast"
                });
                var payByToken = true;

                if (shopData.deliveries != 'true' && instorePickup == true) {
                    getProdss(orderArrayy);
                    $(".del").html(0 + '<span class=""> /=</span>');
                    
                    get_orderArrayy = orderArrayy;
                    get_pointsEarned = totalKobo;


                    //payUsingToken('instore', 'instore');
                    /*
                    if (payByToken == true && !isNaN(parseInt(localStorage.getItem('bits-default-wallet')))) {
                        payUsingToken()

                    } else {
                        $('#ConfirmO').off('click').on('click', function() {
                            navigator.permissions.query({
                                name: 'push',
                                userVisibleOnly: true
                            }).then(function(e) {
                                if (e.state == "granted") {
                                    payUsingMobileMoney(parseFloat($("#totals")[0].innerHTML) + globalDel);
                                } else {
                                    document.getElementById('notificationsModal').style.display = "block";
                                }
                            })
                        });

                    }
                    */
                    $(".confirmText").html("")
                    $(".confirmText").append()
                    $(".del").html("");
                    $(".del").append();
                    //-----------------------------------------------Check if deliveries are on -----------------------------------------------------------------------------------
                    checkDeliveries(shopData.deliveries);
                    M.Modal.init(document.getElementById('modalconfirm'), {
                        onOpenEnd: modalConfirmOpenEnd,
                        onOpenStart: M.Modal.init(document.getElementById('locationModal')).close(),
                        dismissible: false,
                        onCloseStart: clearCart
                    }).open()
                    $('.star2').addClass('animated shake'), setTimeout(function() {
                        $('.star2').removeClass('animated shake')
                    }, 1000);

                }else{
                    deliverItems();
                }
            }
        }
    }
}

function modalConfirmOpenEnd(){
    
    shopData.lastSchedule = false;
    
    $("#totals").parent().removeClass("granted");
    $('.spinnerCheckout').css("display", "none");
    $('.checkoutInfo').css("display", "block");
}

function sendratings() {
    doFetch({
        action: 'shopRatings',
        stars: $('#ratingId').val(),
        review: $('#textareaRating').val(),
        user: localStorage.getItem("bits-user-name"),
        service: parseInt(getBitsWinOpt('s'))
    }).then(function(s) {
        if (s.status == "ok") {
            // $('#ratingId').val("");
            //$('#textareaRating').val("");
            swal("success!", "Ratings and Reviews have been sent!", "success")
        } else {
            swal("Cancelled", "Ratings and Reviews have not sent", "error");
        }
    });
}

function checkmobiveri() {
    //    //Check User Phone Number
    //    if (getPhnNo != "") {
    //        $(".mobiVerificationToast").remove();
    //        M.toast('Please verifiy your mobile number<div class="right verifyPhoneNumb" style="color:yellow;">verify</button>', null, "mobiVerificationToast");
    //    } else if (getPhnNo == null) {
    //        $(".mobiVerificationToast").remove();
    //        M.toast('Please verifiy your mobile number<div class="right verifyPhoneNumb" style="color:yellow;">verify</button>', null, "mobiVerificationToast");
    //    }

}

function checkDeliveries(d) {
    ////console.log(d);
    if (d == 'false') {
        //console.log("Deliveries for this shop not available");
        $(".orderModalMap").addClass("pointer-events");
                    $(".orderModalMap").html('<p style="text-align: center; vertical-align: middle; line-height: 160px; padding: 2%; display: block; margin-left: auto; margin-right: auto; color: white; background: #8c8c8c;">' +
                        '<span style="border: 1px white solid;padding: 10px;">sorry  <i class="material-icons" style="margin-top: 50px;position: relative;top: 5px;color: white;font-size: 20px;">warning</i>   deliveries not available</span></p>');
                    $(".mapText").html("visit store with order number to pickup");
                    
    } else {
        $(".orderModalMap").removeClass("pointer-events");
                    $(".orderModalMap").html('<p onclick="deliverItems()" style="text-align: center; vertical-align: middle; line-height: 160px; padding: 2%; display: block; margin-left: auto; margin-right: auto; color: white; background: #8c8c8c;">' +
                        '<span style="border: 1px white solid;padding: 10px;">request delivery  <i class="material-icons" style="margin-top: 50px;position: relative;top: 5px;color: white;font-size: 20px;">motorcycle</i>  ' + delRate + ' ' + baseCd + ' per KM</span></p>');
                    $(".mapText").html("deliveries are available");
                    
    }
}

function createOrder() {
    for (var o = 0; o < orderArray.length; o++) {
        //console.log(orderArray[o].pid);
        e = getObjectStore('data', 'readwrite').get('bits-merchant-id-' + localStorage.getItem('bits-active-service'));
        e.onsuccess = function(event) {
            //console.log(orderArray[o].pid);
        }
        e.onerror = function(e) {}
    }
}

function getProdss(orderArrayx, costofItems) {
    new Promise(function(resolve, reject) {
        actvServ().then(function(x){resolve(x.list)}).catch(function(e){
        console.log(e)
                resolve([]);
        })
    }).then(function(r) {
        var costofItems = 0;
        ////console.log(r);
        //$("#products").html("")
        delCartChips();
        for (var o in r) {
            var prdName = r[o].name
            var prdId = r[o].id
            var prdPrice = r[o].price
            for (var oo in orderArrayx) {
                ////console.log("------------------------------------->>", r[o].id, orderArrayx[oo].count)
                if (prdId == orderArrayx[oo].pid) {
                    var prdCount = orderArrayx[oo].count
                    if (isNaN(prdCount)) {
                        orderArrayx[oo].count = 1
                    }
                    // console.log(prdName,prdId,prdPrice,orderArrayx[oo].count)
                    costofItems = costofItems + (orderArrayx[oo].count * r[o].price / orderArrayx[oo].unit);

                    try {
                        var srcSetPth = r[o].imagePath.replace('.png', '.webp').replace('.webp', '-35.webp');

                    } catch (e) {
                        var srcSetPth = '';

                    }
                    //TO-DO 
                    // append thumbnails instead of full res image
                    
                    try{
                    var imgLnk=r[o].imagePath.replace('.png', '.webp').replace('.webp', '-224.webp');
                    }catch(er){
                        console.warn('bad Image! '+er);
                      var imgLnk = '';  
                    }
                    //cartChips.addChip({ tag: numberify((orderArrayx[oo].count*orderArrayx[oo].unit)) + ' ' +orderArrayx[oo].metric+ ' '+prdName + '</br>'+numberify(prdPrice * orderArrayx[oo].count,2) + '/=',image: r[o].imagePath, });
  $("#products").append('<div class="chip"><img style="margin: 0 8px 0px -32px!important;" src="'+imgLnk+'" alt=""><span>'+prdName + '</span></br><span style="font-size:85%;font-weight: normal;"> ' +numberify((orderArrayx[oo].count)) + ' '+orderArrayx[oo].metric+ '</span></br> </br> <span style="font-size:100%;font-weight: bold;"> ' + numberify(prdPrice * orderArrayx[oo].count / orderArrayx[oo].unit) +' '+baseCd+'</span></div>');
                    
                }
            }
        }
        //console.log('testing', costofItems);
        //$(".totals").html("")
        //$(".totals").append(parseInt(costofItems))
        finalCost(costofItems);
        //cop(costofItems);
    })
}

function delCartChips(){
    try{
   var allChi= cartChips.chipsData;
   for(var i in allChi){
       cartChips.deleteChip(i);
   }
        
    }catch(er){
    console.log(er);    
        
    }
    
}

bp = 0
dis = 0

async function getPromos() {
   // clearCart();
    //promoModalActive = true
    //clearCart();
    bp = 1
    // 	var lipromo = $(".bpr").attr("id");
    //var w = clicked_id
    //console.log(clicked_id);
    $("#totals").parent().addClass("granted");
    $("#ConfirmO").css("display", "block");
    // 	//console.log($(".bpr").attr("id"));
    // 	////console.log($(".bpr").attr("promo"));
    // 	var xx = document.getElementById(lipromo).id;
    // 	var tt = $(".bpr").attr("promo");
    var pd = shopData.shopPromos;
            var multiplePromo = [];
    for (var ixi = 0,multiplePromo=multiplePromo; ixi < pd.length; ++ixi) {
        //console.log("=============== looping ==============================")
        var pitems = JSON.parse(pd[ixi].promoItems);
        var prid = pd[ixi].id;
        var promoOder = [];
        var numbOfPromo = $(".promoInput-" + prid).val();
        
        if(parseInt(getBitsWinOpt('pid')) == parseInt(prid)){
            var numbOfPromo = 1;
        } else if (parseInt(numbOfPromo) < 1 || parseInt(numbOfPromo) == NaN) {
            continue;
        }
        
        //promotions have to be scheduled
        $("#ConfirmO").css("display", "none");
        
        
        //show discount star if sale is discounted
        
        if(getBitsWinOpt('pid')){
            
        $("#burst-11").css("display", "block");
        
        $(".wishlistChip").css("display", "none");
        
        }else{
            
        $("#burst-11").css("display", "none");
        
        $(".wishlistChip").css("display", "block");
            
        }
        
        
        
        dis = JSON.parse(pd[ixi].discount);
        
        if(getBitsOpt('rid')){
            
    document.getElementById("promoDiscount").innerHTML=dis+'% Money Back';
        
        }else{
            
    document.getElementById("promoDiscount").innerHTML=dis+'% Discount';
            
        }
        
        
        
        
        
        

        ////console.log(w , tt , "ww and tt");
            //console.log("match");
            var obj = {};
            var p = obj
            for (var i = 0, j = pitems.length; i < j; i++) {
                obj[pitems[i]] = (obj[pitems[i]] || 0) + 1;
            }
            for (var key in p) {
                if (p.hasOwnProperty(key)) {
                    //console.log(key + " -> " + p[key]);
                    promoOder.push({
                        pid: key,
                        count: p[key],
                        discount: dis
                    });
                    // loop to get product price
                    for (var iix = 0, j = pitems.length; iix < j; iix++) {}
                }
            }
        
        
        
         var allProducts = shopData.list;
    for (var i = 0,promoOder=promoOder; i < allProducts.length; ++i) {
      
      
        
            for (var iii in promoOder){
            if(parseInt(promoOder[iii].pid)==parseInt(allProducts[i].id)){
               promoOder[iii].metric=allProducts[i].metric;
               //promoOder[iii].discount=promoOder[iii].discount;
               promoOder[iii].unit=parseFloat(allProducts[i].rstQuantity);
               promoOder[iii].count=parseFloat(allProducts[i].rstQuantity)*promoOder[iii].count;
               
               
               }
               
            }  
      
    }
        

        
        
        
        
        
        
        
        
        
        
            var hashmap = promoOder;

            for (var i = 0, l = hashmap.length,multiplePromo=multiplePromo; i < l; i++) {
                var newHashmap = hashmap[i];
                newHashmap["count"] = newHashmap["count"] * numbOfPromo 
                multiplePromo.push(newHashmap)
            };
        
           // makeOrder(multiplePromo);
        // cop();
    }
    
        return multiplePromo;
    //$(".bpromo").attr("id")
}

function clearCart() {
    if ($(".createOrderToast").length >= 1) {
                        setTimeout(function() {
                            $(".createOrderToast").remove()
                        }, 1000)
                    }
    
    //console.log("clear cart");
    $(".bitsInputQty").val(0);
    $(".counter-minus").addClass("disabled");
    $(".star2content").html('');
    tabulateTotals();
    $(".totals").html("");
    $(".totals").append("0");
    $("#dscnt").html("");
    $("#burst-11").css("display", "none");
    delCartChips();
     $("#products").html("");
    $("#ConfirmO").html("confirm");

    $('#shareWishlist').replaceWith('<a class="white-text modal-action waves-effect waves-green btn-flat" id="ConfirmO" style="border: solid white 1px;">Confirm</a>');
    $('#modalconfirm').removeClass('activeWishlist');
    $('#svgHolder').html('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;background: white;padding: 10px;width: 100px;border-radius: 50%;position: absolute;left: 0;/margin-left: auto;*margin-top: -50px; */margin-left:;right: 0;margin: auto;top: -60px;" xml:space="preserve"> <path style="fill:#2986a0;" d="M436.78,485.279C421.056,502.253,398.764,512,375.622,512H136.386 c-23.142,0-45.434-9.747-61.159-26.721c-15.724-16.974-23.732-39.946-21.962-63.028l22.872-288.189 c1.419-18.523,17.124-33.058,35.737-33.058h288.259c18.613,0,34.318,14.535,35.737,33.098l22.862,288.109 C460.512,445.333,452.505,468.305,436.78,485.279z"></path> <path style="fill:#0F5F75;" d="M436.78,485.279C421.056,502.253,398.764,512,375.622,512H255.874V101.004h144.259 c18.613,0,34.318,14.535,35.737,33.098l22.862,288.109C460.512,445.333,452.505,468.305,436.78,485.279z"></path> <path style="fill:#0F5F75;" d="M354.969,98.975v68.946c0,8.287-6.708,14.995-14.995,14.995c-8.277,0-14.995-6.708-14.995-14.995 V98.975c0-38.037-30.939-68.986-68.976-68.986h-0.13c-37.977,0.07-68.846,30.989-68.846,68.986v68.946 c0,8.287-6.718,14.995-14.995,14.995c-8.287,0-14.995-6.708-14.995-14.995V98.975c0-54.531,44.324-98.905,98.835-98.975h0.13 C310.575,0,354.969,44.404,354.969,98.975z"></path> <path style="fill:white;" d="M323.56,275.493l-67.686,67.686l-9.867,9.867c-2.929,2.929-6.768,4.398-10.606,4.398 c-3.839,0-7.677-1.469-10.606-4.398l-36.347-36.347c-5.858-5.858-5.858-15.345,0-21.203c5.858-5.858,15.355-5.858,21.203,0 l25.751,25.741l20.473-20.473l46.484-46.484c5.848-5.848,15.345-5.848,21.203,0C329.418,260.139,329.418,269.635,323.56,275.493z"></path> <path style="fill:white;" d="M323.56,254.281c5.858,5.858,5.858,15.355,0,21.213l-67.686,67.686v-42.415l46.484-46.484 C308.205,248.433,317.702,248.433,323.56,254.281z"></path> <path style="fill:#2986a0;" d="M354.969,98.975v68.946c0,8.287-6.708,14.995-14.995,14.995c-8.277,0-14.995-6.708-14.995-14.995 V98.975c0-38.037-30.939-68.986-68.976-68.986h-0.13V0h0.13C310.575,0,354.969,44.404,354.969,98.975z"></path> </svg>');
    M.Modal.init(document.getElementById('modalconfirm')).close();
    
}
var totalKobo

function cop(costofItems) {
    //costItems = costofItems
    if (bp == 1) {
        //console.log("------> ", costofItems, dis, "<------------");
        var pe = (costofItems * dis) / 100
        var rate = allTokens['bits'].rate;
        var g = Math.floor(pe);
        var kshToKobo = rate / g
        totalKobo = kshToKobo
        //$(".star2").removeClass("displayNone");
        $(".star2content").html('');
        $(".star2content").append(Math.floor(pe));
        //console.log(">>>", g, ">>>", rate, ">>>", kshToKobo);
        bp = 1
    } else {
        //console.log("------> ", "not promo", "<------------");
    }
}

//Wallet State
function walletStatus() {
    if (sessionStorage.getItem('walletKey')) {
        //wallet is unlocked
        //get balance
        walletFunctions(localStorage.getItem("bits-user-name")).then(function(e) {
            M.toast({
                html: 'Wallet unlocked successfully'
            });
            setTimeout(function() {
                fetchRates().then(function(e) {
                    updateEarnedTokens();
                    $("#ConfirmO").html("Confirm");
                    $("#ConfirmO").removeAttr("disabled");
                    $("#chooseWalletModal").css("display", "none");
                    $(".unlockWalletToast").remove();
                    $(".localCurr").html(baseCd + " ");
                })
            }, 1000);
        }).catch(function(err) {
            console.log(err)
            M.toast({
                html: 'Error unlocking wallet'
            });
        })


        window.setInterval(function() {
            updateEarnedTokens();
        }, 20000);
    } else {
        walletFunctions(localStorage.getItem("bits-user-name")).then(function(e) {
            M.toast({
                html: 'Wallet unlocked successfully'
            });
            setTimeout(function() {
                fetchRates().then(function(e) {
                    updateEarnedTokens();
                    $("#ConfirmO").html("Confirm");
                    $("#ConfirmO").removeAttr("disabled");
                    $("#chooseWalletModal").css("display", "none");
                    $(".unlockWalletToast").remove();
                    $(".localCurr").html(baseCd + " ");
                })
            }, 1000);
        }).catch(function(err) {
            console.log(err)
            M.toast({
                html: 'Error unlocking wallet'
            });
        })
        $("#checkBal").html("locked");

        //Check Bal Interval

        updateEarnedTokens();
        window.setInterval(function() {
            updateEarnedTokens();
        }, 20000);
    }
}

// very delayed event listeners
// TO-DO move to passive listeners
setTimeout(function(e) {
    $(document).on("click", ".activateNotifications", function(e) {
        document.getElementById('notificationsModal').style.display = "none";
        $("#ConfirmO").html("confirm");
        //        M.toast({
        //            html: 'Hit confirm to complete order'
        //        });
    });
    $(document).on("click", ".selectedWallet", function(e) {
        $(this).html('<div class="preloader-wrapper active" style="width: 20px; height: 20px; margin: 5px 15px;"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div></div><div class="gap-patch"> <div class="circle"></div></div><div class="circle-clipper right"> <div class="circle"></div></div></div></div>')
    })
    /*
         var wishlistButton = document.getElementById('wishlist');

                    // Listen for any clicks
                    wishlistButton.addEventListener('click', function (ev) {

            console.log('AFTER WISHLIST     ',e);
                            // Get the canonical URL from the link tag

$('#modalconfirm').modal('close');
            //swal("success!", "your order has been sent!", "success");
            //                                                        var toastHTML = '<span>Turn on notifications</span><button class="btn-flat toast-action" onclick="startmessage()">Activate</button>';
            M.toast({
                html: 'Your wishlist has been sent!'
            });
            //                                                        $(".sweet-alert .sa-button-container").prepend('<div id="appendPushSubs"><div class="switch"> <span class="js-push-button-notification-title bits-13" style="">Activate notifications to track your order</span> <label><input onclick="startPushManager();" class="js-push-button-notification" style="background: rgb(128, 210, 147);" type="checkbox"> <span class="lever right" style=" margin-top: 4px; margin-right: 5%;"></span></label> </div><br></div>')
            clearCart();


                            ev.preventDefault();

                    });

    */


}, 8000);


async function getWishId() {

    $('#ConfirmO').html('share');
    $('#ConfirmO').replaceWith('<button class="white-text btn-flat bits" id="shareWishlist" style="border: solid white 1px;" onclick="sharewishList()">share</button>');
    $('#shareWishlist').html('<div class="preloader-wrapper big active" style=" width: 20px; height: 20px; margin-top: 9px;"> <div class="spinner-layer spinner-blue-only"> <div class="circle-clipper left"> <div class="circle"></div></div><div class="gap-patch"> <div class="circle"></div></div><div class="circle-clipper right"> <div class="circle"></div></div></div></div>')
    $('#modalconfirm').addClass('activeWishlist');
    
    var e = await doMakeOrder(orderArrayy, 'wishlist', globalDel, locOrigin, localStorage.getItem("bits-user-name"), get_locStr, shopData.id);
   
   wishShareId = e.oid;
$(".wishlistChip").css("display", "none");
$('#svgHolder').html('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512" style=" width: 100px; position: absolute; left: 0;/ margin-left: auto; margin-left: auto; margin-right: auto; right: 0; top: -50px;"><linearGradient id="a" gradientTransform="matrix(1 0 0 -1 0 -12310)" gradientUnits="userSpaceOnUse" x1="0" x2="512" y1="-12566" y2="-12566"><stop offset="0" stop-color="#00f1ff"></stop><stop offset=".231" stop-color="#00d8ff"></stop><stop offset=".5138" stop-color="#00c0ff"></stop><stop offset=".7773" stop-color="#00b2ff"></stop><stop offset="1" stop-color="#00adff"></stop></linearGradient><path d="m512 256c0 141.386719-114.613281 256-256 256s-256-114.613281-256-256 114.613281-256 256-256 256 114.613281 256 256zm0 0" fill="'+storeTheme+'"></path><g fill="#fff"><path d="m337.605469 192.738281h-90.761719c-8.285156 0-15 6.714844-15 15 0 8.28125 6.714844 15 15 15h90.761719c8.285156 0 15-6.71875 15-15 0-8.285156-6.714844-15-15-15zm0 0"></path><path d="m352.605469 265.363281c0-8.285156-6.714844-15-15-15h-90.761719c-8.285156 0-15 6.714844-15 15 0 8.285157 6.714844 15 15 15h90.761719c8.285156 0 15-6.714843 15-15zm0 0"></path><path d="m278.539062 411h-103.730468c-15.558594 0-28.21875-12.660156-28.21875-28.21875v-253.5625c0-15.558594 12.660156-28.21875 28.21875-28.21875h177.125c15.558594 0 28.21875 12.660156 28.21875 28.21875v136.144531c0 8.285157 6.71875 15 15 15 8.285156 0 15-6.714843 15-15v-136.144531c0-32.101562-26.117188-58.21875-58.21875-58.21875h-177.125c-32.101563 0-58.21875 26.117188-58.21875 58.21875v253.5625c0 32.101562 26.117187 58.21875 58.21875 58.21875h103.730468c8.285157 0 15-6.714844 15-15s-6.71875-15-15-15zm0 0"></path><path d="m435.011719 321.253906c-8.449219-8.554687-20.015625-13.261718-32.566407-13.261718-13.519531 0-23.679687 4.585937-30.96875 11.128906-7.289062-6.542969-17.449218-11.128906-30.96875-11.128906-12.554687 0-24.121093 4.707031-32.566406 13.261718-8.640625 8.746094-13.402344 21.035156-13.402344 34.601563 0 28.558593 24.140626 46.554687 43.53125 61.011719 8.742188 6.519531 17 12.671874 21.722657 18.539062 2.847656 3.539062 7.144531 5.59375 11.683593 5.59375 4.539063 0 8.835938-2.054688 11.683594-5.59375 4.726563-5.867188 12.980469-12.019531 21.722656-18.539062 19.394532-14.457032 43.53125-32.453126 43.53125-61.011719-.003906-13.566407-4.761718-25.855469-13.402343-34.601563zm-48.0625 71.5625c-5.234375 3.902344-10.5625 7.875-15.472657 12.066406-4.914062-4.191406-10.242187-8.164062-15.472656-12.066406-15.46875-11.53125-31.464844-23.457031-31.464844-36.960937 0-11.019531 6.117188-17.863281 15.96875-17.863281 8.132813 0 11.265626 3.128906 13.085938 5.855468 2.246094 3.363282 2.84375 7.117188 2.933594 7.785156.433594 7.851563 6.855468 13.824219 14.738281 13.941407.070313 0 .144531.003906.21875.003906 7.820313 0 14.335937-6.238281 14.945313-14.058594.003906-.035156.5-3.695312 2.550781-7.085937 1.816406-3 4.988281-6.445313 13.460937-6.445313 9.851563 0 15.96875 6.847657 15.96875 17.867188 0 13.503906-15.996094 25.429687-31.460937 36.960937zm0 0"></path><path d="m205.0625 207.738281c0 7.957031-6.449219 14.40625-14.40625 14.40625s-14.40625-6.449219-14.40625-14.40625 6.449219-14.410156 14.40625-14.410156 14.40625 6.453125 14.40625 14.410156zm0 0"></path><path d="m337.605469 135.109375h-90.761719c-8.285156 0-15 6.714844-15 15s6.714844 15 15 15h90.761719c8.285156 0 15-6.714844 15-15s-6.714844-15-15-15zm0 0"></path><path d="m205.0625 150.109375c0 7.957031-6.449219 14.40625-14.40625 14.40625s-14.40625-6.449219-14.40625-14.40625 6.449219-14.40625 14.40625-14.40625 14.40625 6.449219 14.40625 14.40625zm0 0"></path><path d="m205.0625 265.363281c0 7.957031-6.449219 14.40625-14.40625 14.40625s-14.40625-6.449219-14.40625-14.40625 6.449219-14.40625 14.40625-14.40625 14.40625 6.449219 14.40625 14.40625zm0 0"></path><path d="m246.84375 307.992188c-8.285156 0-15 6.714843-15 15 0 8.285156 6.714844 15 15 15h21.628906c8.285156 0 15-6.714844 15-15 0-8.285157-6.714844-15-15-15zm0 0"></path><path d="m205.0625 322.992188c0 7.957031-6.449219 14.40625-14.40625 14.40625s-14.40625-6.449219-14.40625-14.40625c0-7.957032 6.449219-14.40625 14.40625-14.40625s14.40625 6.449218 14.40625 14.40625zm0 0"></path></g></svg>');

    
    return e;

}

function sharewishList() {
    
    if (navigator.share) {
        // Share it!
        navigator.share({
                title: document.title,
                url: '/bits/index-'+shopData.id+'.html?w=' + wishShareId
            }).then(function(e) {
                clearCart();
                return e;

            })
            //.catch((error) => console.log('Error sharing:', error));
            .catch(function(err){
                clearCart();
                console.log('Error sharing:', err);
                doFetch({
            action: 'delWishID',
            id: wishShareId
        });
                M.toast({
                        html: 'Wishlist deleted!'
                    });
            });
    }
}

async function insufficientOrder() {
    if ($("#mobileNo").val() == "") {
        M.toast({
            html: "Please enter your mobile number"
        })
    } else {
        $("#insufficientOrderStatus").css("visibility", "visible");
       var ordAmt = await tabulateTotals();
        doFetch({
            action: 'setInsufficientFundsOrder',
            transactionCode: $("#trnscode").val(),
            uid: localStorage.getItem("bits-user-name"),
            amount: ordAmt,
            num: $("#mobileNo").val()
        }).then(function(e) {
            if (e.status == "ok") {
                
    bitsokoUID = localStorage.getItem("bits-user-name")==null ? 0 : localStorage.getItem("bits-user-name");
                doMakeOrder(get_orderArrayy,
                    "mn-" + $("#mobileNo").val() + "-" + $("#trnscode").val(),
                    globalDel,
                    locOrigin,
                    bitsokoUID,
                    locString,
                    shopData.id).then(function(e) {
                    $("#appendPushSubs").remove();
                    //$("#products").html("");
                    delCartChips();
                    if (e.status == "ok") {
                        M.toast({
                            html: 'Your order has been sent!',
                        });
                        clearCart();
                    } else {
                        M.toast({
                            html: 'Error try again later!'
                        })
                    }
                }).catch(function(err) {
                    //failed Order
                    M.toast({
                        html: 'Error!! Try again later'
                    });
                    clearCart();
                });

                document.getElementById('insufficientFundsModal').style.display = 'none';
                $("#insufficientOrderStatus").css("visibility", "hidden");
            } else {
                $("#insufficientOrderStatus").css("visibility", "hidden");
                M.toast({
                    html: 'Error! Enter transaction code again'
                });
            }
        })
    }
}

function payTabOn(){
    console.log( M.Tabs.getInstance(document.querySelectorAll('.tabs')[2]).$activeTabLink[0]);
    
    
}

function placeLNDlogo(){
                        
    document.getElementById("doPayQrcode").innerHTML='creating invoice..';
    
    document.getElementById("altPayQrcode").innerHTML='<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"  style="margin: 10px calc(50% - 40px);opacity: 0.6;"><image xlink:href="data:;base64,iVBORw0KGgoAAAANSUhEUgAAAPkAAAFJCAYAAABZ8otRAAABhGlDQ1BJQ0MgcHJvZmlsZQAAKJF9kT1Iw0AcxV9bpUUqglYQcchQnVoQFXHUKhShQqgVWnUwufQLmjQkKS6OgmvBwY/FqoOLs64OroIg+AHi5uak6CIl/i8ptIjx4Lgf7+497t4B/kaFqWbXOKBqlpFOJoRsblUIviKEfgwijJjETH1OFFPwHF/38PH1Ls6zvM/9OXqVvMkAn0A8y3TDIt4gnt60dM77xBFWkhTic+KYQRckfuS67PIb56LDfp4ZMTLpeeIIsVDsYLmDWclQiaeIo4qqUb4/67LCeYuzWqmx1j35C8N5bWWZ6zRHkMQiliBCgIwayqjAQpxWjRQTadpPePiHHb9ILplcZTByLKAKFZLjB/+D392ahckJNymcALpfbPtjFAjuAs26bX8f23bzBAg8A1da219tADOfpNfbWvQI6NsGLq7bmrwHXO4AQ0+6ZEiOFKDpLxSA9zP6phwwcAv0rLm9tfZx+gBkqKvUDXBwCIwVKXvd492hzt7+PdPq7wduCXKljwvfxwAAAAZiS0dEAAAAAAAA+UO7fwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB+UIEhURBXljQkAAAAAZdEVYdENvbW1lbnQAQ3JlYXRlZCB3aXRoIEdJTVBXgQ4XAAAcvElEQVR42u2deXRU5d3Hf8/MZJLMcidACKIoKgqiKBRZCijIZhYiMUKIWG1d6oLa11de1Hrac97Tnr7H7dTW1qW1b+vhba0kIUAIIRuBQIAYFjdwQ1QWBVmTubNkkszM8/4RRIMkTMLdnnu/n3P6R49k7p3f7/nc3++5c5/nEgEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPQjWDGGhxvvexGRAN+HIQTmQS5inIh3/h97OrGkUWSThv/aPf31/0F0IDkwheTfGn62TDuIPNNJyqlFziE5ELJVr77n9XjLG/f36o9SRpKU9zHGACQHIhBYNY7zth3n9yE2J/GBeYd9N5VciIhCcmC0Vr04mRNvU+4D7RnEkseQzXv1je6b/rgZEYbkwMjzcUVGioPsA/MPu6ejyouGDSEQn1DtjVz1g/AoERv4a0QblRyYsYqfQirEeEElBwBAciDqSHEiBpAc6IG//u6lWhyHp2Wpc/6b8Bgu5uSg5/l4SSqneKuQ83F/46MF7MDLxV0uJr4J5MvahnEJycFpyQW+6SYvz+AUO9rNyPQQpYwlae4mjNHzxIEQAN3oTnAiIh4kat3U5SJmc48hT+57kB5zcgtV8fWLR2tyoJQrDfF946H3SC4i/t3/7Dy89f7fYCSgXTev5GWjOEV2C9mqKz/VYETkJO69jnw52zGuUclNQuRDYYeIf+0NCt9L4ETURiywvbPaF6dwuXwqxyCB5IKjwRhOukqdFjKg8poXHiEKb8IQgeTiEqxcsE+TAyWP2o9oQ3KgRw2PfDpUk+P0v3wkog3JgR6Sd+zR5Di+Sc8q/qSNXHmrJnNlnj4HAwWSi2y5+k+5Medl6nxwaIs2F6iZFbjLDsnFxF+tzV1jW//Mx1T54Ngx9S9Q9gEYKJBcXFhLgybHcU/7y58Uv0Ctn39ckxi5JmKgQHKhe3VxL1AnP9GkxHpy1qJVh+SgZxtVWtYQ/xqxheSgx3a34bEZWhzHnp6pUhPSrMEFyo6BYhXJ/U1Pmy7b7PjKOk3m4zOUvzPtf/u/NPnN3SZNgdlWkZzteyYqFxGXV17B5bqH/sMUX6r9gLj5OFTykSbz8SysQf8+1lhP3r6X6Pjel+Qieqnz0uYk3i+33TdrRTKGgIZ0HEAMILmCrWHNHE7NFWf/j/F2YidWOLtuSHAteXJ3oQIQEdm8RBRAHNCuG7w1bFnXq38fD+06tUTRxgOrxvBgtTYbJPYGuXyyNo+DXnz3rcLm3TkIVp8ZE7N+MSU3JGDJV5Ctf9YC99SXS3T9TsVJnHiH6sdRZdPGyhmcyetVP/eky58clDr++aNQG5KfZ/+TRPb0OeSevoqZ5judroTDyZu/h4l47mpdoNCuG1Hw6rvKVT1AvINiR1d12W8sUHa5qoM4VDVHE0lY6viN0MJcmPPGW+v2XK0PySNfdFYrZiPmvIZY6thlnsylCxW7roS02bvBk/XmTdACldz4tH2i37F5nHjbLoq3LL39dKVfPe68qzCP7RM2HcFqbXaxcWRkw2grSO7ftmSY8TqLHV23El7ej/vrezml4EH1z1Ol5Zk8uEmTXWxc0ysxH7eC5Oxg/V7Dn2SsmdiRf+Z2Eb80rdtqH27QZnmmvf9sVfZ35tFv1M+73QubLdOui7rKKdpyutoHVl3Fg9ULd57+T99UaLI80z1j2bVKf2Z4c8EhTS7u7mmwubvYmO0LyUV2ThRDZvuAKu87K0rmRG1CnjsquXF7YWTVUHQgBJBcOfyV4/HGjL4OBN84teZPCC4kV3DuIW9HRvuIJ2uHsO0u1o9brl0HRiGwergmnZUnewvm45AcnHuq82PFheStexBYA2Cax1pDm+55PXb4DWS0z1Odt08tImFEtkHE7EPJnnHVMtcNyj2aC3TKrVm+iFyawSmKFYbqjBIH2bwzyZNd3avxosnKM8cAkuadQLtuiXYdgqsHj1Jcru666m7lYB7evOihbgWvvFmbDS4GFd6HBFmlkhcRfj7TG/dYknLfYVrmAw/BWKSSh+oe+QlSaYREvHO62mtTolIQc6tIHg82/AuptGIfmoEYWEVyHvkAmbQivmsQA6tIjtfiWBPpZqwft4zk0oIYkwqJMfdwZNQyI9eFGCRaA8385fw1uZy1VBHxKDJtNjxZJM2pQiW3uuRnEtrw4JK4v+YF3vYlMi9694afziB5IgTK58nU8YmXR/eQFi8tAAoOXLuHki69Nztl3J+qEA1Innh7Xz6cszAWVYg1gp1kc98S8MwplRAMSN4r5NWjObW+j0CIhC2ZKP2BW6Xpfy5DMCB54rKv/BGn9ncRCNGwDyZp/mGMb0ieYBtfOZszuRaBEBX3JJJyGxkkBz2LvuXJDPbV80cQCVFHehKRK5Ok3DUMkoPuRa+azZkfFV14UsaTlLedQXLQzRx9PKd2bBhpFqzwezsk74voWLtuOpKuWDwm9foX34fkgIiIAquGcN72FQJhNhlcU8h7i/l2foXkqObgDGze2eTJqTWNG9iSGYAziAdqSS4i7l93WxskB8DM7fuJFU652MshOQBmhgc6962rmMUhOQBmJrhO2PswkByAXiAXEQ9WizVXh+QA9JJ4ywqnvHwwh+QAmJnYYZKLHRySmxR/PV7mAIiIR4WYp+NhmL7My1ZexKn9awQCfFctvSPJk/OxIX1CJe8LEBycOU8PfEzBmjwOyQEws+jNZRTa8PC9kFz0Vn3d4xMQBdAdsaOv/t1fNf84JBeZUEMTgpAgzE42KY+IOaz1tf3LBwRrfswhuai0YVPHRJEWxJgnu4xZ8Q028ea3UcmFhccQg0QEP7XjSuv2B5dYdmpnkJ/XIHkv8JePwhryc5LUZUul6NcVL1g5GoHSdA7JRZprte5HEHocTRkkFXZ0+a2Yt31j7cYvepwCq/VdwQbJe5WxEGLQU4tecPQsD4NgesNb61DJxSGOEJx1FCX3sOspZjhEXNf5OSRPdD6+Lg+j9WzYh5JU0HZWwVu3LR6NAH1HqP7upbpMMxH6xMDGjWfBOZyk/D3djqFAxQjOg58iTt+f0uiwzzsqOeij4EN6FJyICIKfpZqvv/dlSA6MPwX33URS/lfoAvtA7PjSRyC5AQlvXTIMUTg1YPrf0ejJqofgfYXHSK7I4ZDcaFffYyV7EQUi+6D7f+uZ/e/JifxbueHJDESsu559k6aHcyDiCVx8I3gIpvOG0d8S/4OWOrzmudsBFUS7DowE69sd4fBOhK6nTmflRRySG6WzWv/EEOtOwFNJKuSYf6uBhrsLQfJzEA81HLRsi17QCsHNcK1GCM4xfWp9x8Jz8D62olXa/xYsIv5NjxZAckNY3m6t72sfcv5PZUV2PIKBc27YyTXFkFxngmtHWutRVscokuYr8JBL+4cYPInQtg/tuu7z8cDHFqrgLpLm7VZmDo7dczAnB0YTPJWk+WFFBPfX3oyFPJAcGGoApBUcluYrdxednaxBUCG5GPjr5smmT75vLnkySy5Ets0NHmvtriKdWOM1dYeeMZfc01fjd3BUcgvD28wr+EAIbhRCGx6bAcmBokiFxNwz1BE81HAnJuSo5GIg16t/ddVhAqL61kOxQ/+ajdEDycWgpbLOfIJjoQkkB9/RtsdEGZYguIFxT39pPSQHfcfuI6lAhuCo5KDLfLxq0UPmyGx/kub7NRM8VFdwCKMHkotBZMdr4mc1naSCk5pW8HjLpsEYPJBcDNo/EDuhnmySCo5r3qLzKLZ0g+SiIPBDMDbvXPLMqcQcHEDy7ghWifv+cZZ8FXly9HmKLVj3VBpGTx+wuyC51sT9u8W8UvtyyHvrJ7pVcN76fjNGTx/ilpYdgOTg3MXgoqf6ebLW6tuit3+ERPQB36xSSYvjYBWawHQ+pvqc/hWp4wCSgTm58Qk3PnyvUK3eBfNPIGsgEXAn9hSB0gs4j34jUAU3SNxWX8Z565cYQAbOIyr5t5URgvctbhC8b3HLWKDZC/YguTA9l91wgoO+N9C+6cWXQnLQVfAFMQhuFmwDtT0cIk4UrJhk3IdgHBkQ3Gwkj4HkWhMPNhrzgu8eT9K8o4YVXF4zBnus9wFpbg2D5ICYczh5crcbu4KH3kOiep3XEdoXC8u36rXG28/N5plN3vw9aNFNiDf/UwbJNYa3vmeo/dzs6T9b5plTC8EBJFdM8jbjtJyOi552uGcuXShC3OSNWHnW6/wOXrRIl+NaPvJxvwEmanYib/4J1w3PiPM60ODbWHnWS1xTX/sLKrnGhDZkGuLusLQgxqTs5eliBW8LrO2NaGkT0a7rQexotf6Ci/oUG4/C3ESxDyJPZhOD5FaDJQsruL9h8WgksBcX8vlHdM0zJNcl6oNJWtAm7B105j9YiSQmmGr3Dfqfg1WDH3778Qm6XdkLDov9E1nH59h+OUE8uZsZJNdrPn64tEn7aKeYYyVZx8ewN5GLuUFybVnJebu2WxYxxwiSCiKmeMiFJQ+Fweei/09XGCZfVs2BXESa/XzGUsaQN+89U8Y6VJXLY6GDRPGviOIniQhrVmyebEPtfw/JVRd8KHnz9lsqzv7KSZzJjRYVfCJ55jQZKt+WlFwun8QprMEg9N5IUk4DnkMnIn9VFmf+KnML7p1Mnpythsu3NSXXooqn3dEoZf57MvTunuDa6TweaiKKh8UXPK3gsCez5EJDnhuGmgqk//xZCH5uPDkbmFQQZlIhsaRLH3yCbFcRsXQSa0kFI/uAOxqNKrglK7lc+dNSkv/vNrU+H5stKke46dGC6NclxdRh3DemipBv60m+cgKn9m2qhFIq5BBc7bl9/cP3Mv+6v1Nkj74tsDSRPNlNQuTbepKXeDnFFX7PHEslaUErBNetO7tjKwWKJhFXf6Uuc44ib/5uoXJtPclVuenGiDzXEx80y+Ub92wrtDNLXrviuOC2dte0FcmixcZav9+uy+XsxBr1D5R0GVHq+P1StnYb6Fta8BI3p3hIpb7cSXzwfw7y3fD8UVHjYynJtXzK7YfiX0A8LXOZb4YY2zsJk9NiJyferrAVNrKlzSbPzdXmeAwZkuuIPZX4gLl7fNOLRkBXnfNp70c211Ty5JSZzglIbhzjiewXEbNfQvZBVz3rmvy/T0PjsxNu/GVq9MBzYSWek2fODPLmHzW1B5aRPNz0VFp033NibT7IHGRzTyTPnC24c/9tHrc8MST61QsHFQ2zaxp5b9nIILngBMqGcR753BTfxeabQp4s64kfqr2Zx07WqBfXtBnkyVyPdh2tujHhqVeSb+5nps1nsHoKj7dos0OsY/DD97mmvvoPSA7JDVzq04gcI0jKbzJFfgNrbuQ81KDxTOlK8s4zx0UTC1TMSLyFqL3JFF9FXn2D5oITEfHoZyQXEQ83PJkByUVo9dZOw3YlIgpeksKpdbOu5xA99PyRQNl4DsmNXtgCG61nSNJA8adX8YghzoVHtgs93UO7blL4oHmPCSv48ssMKZRcRNxfd/dS0eJp+htvgaqCQ9xfYrl9wkVd1y4X+zhxv7FP0jcvIGWVSqjkRqlooT3WexEAc4nbohtdcCIif6k3sNzFIblRiH1mPccdl4spuEjFIxYmUUQ3v+Q8bDnJyTFMsBY9ScibWjwWpsDqmzgk17OrWj9PJgviuGikMP26XMQ48Q5xa0hrPQWrjT3OTC05O1bqtaLkqdcbf3ea0KYHl3S26OI/whBvKfWGao1b0R0EzIUthYgihj7FYO3CnbHDfx1rprDHTtajkgONEuqZauz2vGwij598a6wZYy8X2zgk13I+3rBkmBUl92TXGPb3cbl8AqdIk3mDz+MUKJ/MIblW8/ETq/airhtI8BWDOYW3mf578vBWCm1c9BAk14I2OG4YwZcP4NRx2DLfN3bkr69BcmAdwYuTOMVOWOtL8zjJK0dySA6UH1u+GcYSvMQp9G/g50X7x6jkqg6u8hstuX7cl2Wc/ck6l4q2k5UJrLmGQ3K1CG8moA+t2x74lVyUhE06iIiHPkQlVzG8GGE60fHl678j6kAgThHatHg0JFcYf8UsGK5niw66EDvy2nuQXGFY+AtrtobSOP3mnhvu/w0E74a4/ssITLczjFyczIm3WW4s6bUTjFw2gVNkG2TuSTJ3Hnlz9XvHmvnm5BYUXLcLatU9r0PwBGj7AO26YvPx+oU7LTmI7JdoL3hFLif/G/fD4ATqTvRLSK5YW3S8bKwlR5HGO8HIpcM4BdfA3l7QuvPxCZBcCWJha44g1yXLNBN8+QWcop/D2t4OzWPrdVt+Z6obb1a9w6vVTTe5dDCn6GEYa/A8mbuSAxUFHwDBBcU0kgfKr7ZkFWfJ6s/H5SIbp+gJ2ALJ9YWHP7JmAn3TXlFV8GIbJ4rDFEh+5pWf8WB1jmaVNbBmtmWftnJP/8ej6lVw4sQhuOgovlurXJLCKR6heMvaLjfCmOtq8t7ykeI3HkIb5vLY0dXIpIL4G5+2swPPRBEJSH52unndLA9/9IO733xQ4R7fTUUj+nqo1u2/TO344llkUUnB627h7MAzCISJULSyBitn8rhc1/s/tKcTJY8j7rrmat/M3ye0pUawuuBQvMV6byvtkrykdPLedlyxHPqrCz9lLUXDoYU66PUTmqIHVfR3au8sknLW/eD8QjU5PNZSQ8TRTTqG3P9b15S//bcygt9Vzlr+mQsVIXn3glfe9yLJf38cqRRv0Mill3HS+flq5Es9lLu7HnwTgguIvNwNwU2OIpL76ws/7e6GGzCw4CvHcYqFEAhInkDPfwQ3a7TPXPJ5/XmgbDKn9h2IIyQHRsVx0bzGPlfwIuI8shVBhOQJVoTlbuztpQOuyf+e3FfBET1I3is45nQ6ZM3dtwpegguybinzXS+m5KF6Y7290Sow5zW9F7zYwSmOC7Ju06uMWf2ElDx25LXXkD4dJE8e2audYDoXmuDhIT1JGftci3CShzY8NgOp0wdP1tKFCcld/2gB5uDiTq90lzzeXFmH7BkXef3D99KRl4sRCQN0XklX6TtV6Osf8o49yJ4ul+UUIur5wSN/5U2cjr2KWBlmenXlbiL9dgvvUyVHC6hn59fzXVp/xQTO5HoEykjTq+xl1wpZyYFOAyZnS7eLHOSy6zkF8UYTcJ6VPFiVhSpuxDl42QROkZ0IhNFIuU48yeP+KiTOYASWp+ClgwZFyvuACSc50JMfjhe5iDiPYQUgUEhyuVhCq64j3D2qaz6WX4p8AOUk929+Ko24jIjpiC93F/vuguvhFNuHoBj5opx+R6NQkrOjVc1ImzHofEw1iEAY/aI8s28rBZUm8Z/QOt5H1nTGXzmOMxkbPQAVKrlcegnmfgYAgovUqs8VbE4ePYCsAZDw1dhBvpmrmVCSS4XEaMCiRZR8NRIIwLkcTzXWAs0+XW38TU9msG/eOUIdXxLF9+NFBwCcWRRFl/ys4lf+mDP5bWQYWLuKOweTN/+wOSX/gfR1d5Wzlspcih5H5gGquBklPxO5dtFDFGp4jdp2YyQAc2JLIakgYl3Ju1T5bUuGsUP1e6nja6L4ESLCi+6B+PCLn7jYN/mFryB5N4TWzeaxE7UYKQCtutINhlFOxD2rlkmFdPp/jsEPPsGcgzFygBhc/FQ/o54aEyWGcs2DSyi89QVq24UBBYyF83qS8ncySK7knP7tx2awr/9ZR9GTGGAAbboZJT9rpS+/jlP4A4w4oC397miUbjbGajPTS34mwZpsHm+uxCAEKtqTRNKCDsM7xKySj0DppZxH92FgAsu06d9imT3emPcSjEqgHA6vMKdqGcnjLVswMIEyBSNlHEnzAgySGw0ew+gEClTw/uTN2yHUNBdbMgPQm3n4vJPC3ceyhORyVcEhDE9w3oIXinmj2hqVPNSA52PB+TFk8RhRT90akke/0SCSvs4rvZRDZHNBChPBL3h8ojTlxfchuUHxb7z3ZU0O5Bzd2dJlr2VSQZhJhcSo3/2/ZSk3ENkkmCJwi+6b9gehXzRn+odh5JU3cmpvUP9A/R65U7r5lTd7+ifBmik83oyf8kRRQyrkzBzfxOySLx/KKbZfkyt+r28VbFy4M35szVgeC8ApI2EfRNL8I6Zxw/ySF9m46jvPMBtJC+LnHcvA+kd+wv01/6L2zyCaXngX7Jdyii8101eywI03DbaWco1TZnzNeOVNKf8zJhUS4xfetQbGaVnuPMQv+PmzZhPc9JVcXj2SU+vHhmzVE+tCCK+n0oLkESTd+qlpXXCYOnkaCK4WwTWTeTy0FQKqWeHsKeSdH2FEn5r6ezqQaoNOMsJfIAhq4byGpPwPGVHEEl8Xz66fbzVw9Ffng/kxBFfpman9cuIXLx7TKbh1MK3koQ3aPK9uH/LTG9X5ZKyaUwz3BJIKOZPmf8F8k8V9cg2Sn9nunqjU5Hl118Q/blb8ArWpUJNJoj399t2nt8Ae8sidLPVKs7RXxPvNpdNbfOduY2RhTPvltbozrcaddSOdu1xz34vUsf9xat9L1L7PmMm2p5PNM4k8WeWWlhmSQ3JFz92/8fZdJO8dxSLHiaidiIeIqPXU5hxqTDVsRMxJRG4i5iObK508c7ZB6gQw5d11ueZnb1HzUvULyMBZRLTOkgPHN23ZtYn+29YdS4YRjw7mscjYuP/dl2KxHt5nn3rlbnL2/wOxpH2+yS+t/94EjDrvhkeI6AQR4dcHS1dyecUwTh2fC1nFw1ufSosefK5Z9QtU2lRyZ25CJbQA5rzxpoHgZPOp8rGxYyuatQgRBIfkwuLf8sCvNGmBUsep8rk8gsUpAJL3LN/Rd36nxXF46pirxY2SEyMfkgtM9GtNDiPN/L3iD8aHNvwyVZOTdwzEyIfkAhPXYD83le5XxoMNYU26kAtvvRNDH5ILjAY/MbvU2biThxs1iZBvUs/bVAFIbliCleO1eYjklndVujOtwQYXzuEY9ZBc4E5d3i7uBapitDYbRLim/A3DHpIDPS5QQW0WR0mZbzyAaENyAAAkNx6hDXdoslcSz1j4jrBBsnsx4iG5wO3uiTWTtDiOb/pb1yv9meGGO2s0uUClZe/HkLcepnl+GUtL9Tl3gEoOAIDkClTCqp+9pUm767oWIwZAcl1o3Xq7JvPxW3Yp36pvXDxak0RLEzHaIbnAaPLuMLs6H9tc/p4WIfJkN2E+DsnFxL/1wSWaHCh1gjqf2/YZ0gyQ/Z5gh0te0EbyyeKuH7cNwkiH5OIizTvJvt1fmzyZRCxZnePMVn79uFz9izxNguS4GCPdwphqt1ZpTvXpeWdow4J98ZOfDOXxfURx2ZgnHGlapcVh+KBx9xFtw2i3KJa5GdPa9Iu86MF/rOKxUO//OPkKkm7dq/yd9WInJ96u/sUPD8GgkluB1Il/LjvzohasnMA7l6f2/MAZ7z93DNGLKpRY9QUn5xVEtBcjHZJbE0921zdwyBWZnMKNP2jvfVOVf0mev+JHnILvqv8l3VNfgeRo18HZJKydxVnzDiIeJKkwiufVASq52fDNXgc5gCnAUxKm7tNwDQeQXBfCm+95XYvj2PtnItgA8zU9kIvsXJ3X+2I+DlDJDUIMIQCQ3Mx8+xiuvX8ukS0VAQFo1y3TxlfkcYrsUOR9bizlMvLmfYn8AkhuVPybbt/FjpSPonioT3+P+TiA5CJW+vLrOIU/SCitUiFHbgEkF51QbT6P+2voB4tu7ENJmr8fuQWQ3EyENz8xJObfc5Ai7xA5RpA3D0/sAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA0Jv/B7Y/QtcUmE1kAAAAAElFTkSuQmCC" width="80" height="139" /></svg></span>';
    
  
}

function selectPaymentMethod() {
    if (checkanon() == false) {
        M.Modal.init(document.getElementById('loginModal')).open();
    } else {
        document.getElementById("chooseWalletModal").style.display = "block";
    }
}

